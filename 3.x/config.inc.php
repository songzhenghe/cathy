<?php
$config=array();
//基本配置
$config['base']=array(
	//网站域名
	'base_url'=>'http://jessica.com/',
);
$config['db']=array(
	// 主机地址
	'hostname' => '127.0.0.1',
	//数据库端口号
	'hostport' => '3306',
	// 用户名
	'username' => 'root',
	// 数据库密码
	'password' => 'root',
	// 数据库名
	'database' => 'cathy',
	// 数据库编码默认采用utf8
	'charset'  => 'utf8',
	// 数据库表前缀
	'prefix'   => 'szh_',

);

return $config;