//https://www.npmjs.com/package/watch.io

var WatchIO = require('watch.io'),
    watcher = new WatchIO()
    ;
let cp = require('child_process');
const fs = require('fs');

let sh="php " + __dirname + '/server.php start';

// Watch a folder recursively
watcher.watch(__dirname + '/app/');

console.log('watcher begin to work!');

cp.exec(sh, { encoding: 'binary' }, function (err, stdout, stderr) {
    console.log(stdout);
});

console.log('start php server ok!');
// Listen on file creation
watcher.on('create', function (file, stat) {
    // expect( file ).to.be.a('string');
    // expect( stat ).to.be.a( fs.Stats );
});

// Listen on file updating
watcher.on('update', function (file, stat) {
    // expect( file ).to.be.a('string');
    // expect( stat ).to.be.a( fs.Stats );
});

// Listen on file removal
watcher.on('remove', function (file, stat) {
    // expect( file ).to.be.a('string');
    // expect( stat ).to.be.a( fs.Stats );
});

// Listen on file refreshment
// The `refresh` event emits when the WatchIO scans a folder
// which usually occur by calling the `.watch(path)` function
watcher.on('refresh', function (file, stat) {
    // expect( file ).to.be.a('string');
    // expect( stat ).to.be.a( fs.Stats );
});

// Listen on whatever the file changes
// the `type` parameter can be any one of: 'create', 'update', 'remove', 'refresh'
watcher.on('change', function (type, file, stat) {
    // expect( type ).to.be.a('string');
    // expect( file ).to.be.a('string');
    // expect( stat ).to.be.a( fs.Stats );
    //使用nodejs执行php
    if (file.indexOf('server.pid') != -1) {
        return;
    }
    console.log(file);
    console.log('重启php进程 ' + new Date());

    // cp.exec("php "+__dirname+'/reload.php',{ encoding: 'binary' }, function(err, stdout, stderr){
    //     console.log(stdout);
    // });
    
    let pid = fs.readFileSync(__dirname + '/runtime/server.pid');
    cp.exec("kill -9 " + pid, { encoding: 'binary' }, function (err, stdout, stderr) {
        console.log(stdout);
        cp.exec(sh, { encoding: 'binary' }, function (err, stdout, stderr) {
            console.log(stdout);
        });

    });
});

// Listen on whatever the fs.FSWatcher throws an error
// the `err` parameter is the same as the one from 'error' event of fs.FSWatcher
watcher.on('error', function (err, file) {
    // expect( err ).to.be.an( Error );
    // expect( file ).to.be.a('string');
    console.log('发生了错误：', err, file);
});