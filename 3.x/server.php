<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;

define('DEBUG', true);
define('ROOTPATH', dirname(__FILE__).'/');
define('BASEPATH', ROOTPATH.'framework/');
define('APPPATH', ROOTPATH.'app/');
define('RUNTIMEPATH', ROOTPATH.'runtime/');
define('LOGPATH', RUNTIMEPATH.'log/');
define('PUBLICPATH', ROOTPATH.'public/');

//包含配置文件
$CFG=require_once dirname(__FILE__).'/config.inc.php';
//db
$db=null;


if (strtoupper(substr(PHP_OS, 0, 3))!=='WIN' && DEBUG) {
    require_once BASEPATH.'FileMonitor.php';
}


// #### http worker ####
$http_worker = new Worker("http://0.0.0.0:2345");

$http_worker->name='jessica framework';
Worker::$logFile = LOGPATH.'workerman.log';
Worker::$stdoutFile = LOGPATH.'stdout.log';
// 4 processes
$http_worker->count = 4;

$http_worker->onWorkerStart = function ($worker) {
    global $CFG;
    global $db;

    //save server pid for nodejs reload
    if (strtoupper(substr(PHP_OS, 0, 3))!=='WIN') {
        file_put_contents(RUNTIMEPATH.'server.pid', posix_getppid());
    }
    
    //错误处理
    //https://www.php.net/manual/zh/function.set-error-handler.php
    if (defined('DEBUG') and DEBUG) {
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            echo "在{$errfile}的第{$errline}行发生了错误：{$errno} {$errstr}\n";
        });
    } else {
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            $str="在{$errfile}的第{$errline}行发生了错误：{$errno} {$errstr}\n";
            file_put_contents(LOGPATH.'error_log.txt', $str, FILE_APPEND);
        });
    }

    //引入自定义函数文件
    require_once BASEPATH.'functions.inc.php';

    spl_autoload_register('my_autoloader');
    //引入项目自定义函数文件
    require_once APPPATH.'functions.inc.php';

    try {
        //连接db
        $db = new \Workerman\MySQL\Connection($CFG['db']['hostname'], $CFG['db']['hostport'], $CFG['db']['username'], $CFG['db']['password'], $CFG['db']['database']);
        $db->query("set names {$CFG['db']['charset']}");
        $db->query("SET sql_mode=''");
    } catch (Exception $e) {
        echo $e->getMessage();
    }
};

$http_worker->onConnect = function ($connection) {
    //echo "new connection from ip " . $connection->getRemoteIp() . "\n";
};

$http_worker->onClose = function ($connection) {
    //echo "connection closed\n";
};

// Emitted when data received
$http_worker->onMessage = function ($connection, $data) {
    // $_GET, $_POST, $_COOKIE, $_SESSION, $_SERVER, $_FILES are available
    // var_dump($_GET, $_POST, $_COOKIE, $_SESSION, $_SERVER, $_FILES);
    // send data to client
    global $CFG;
    global $db;
    //https://blog.csdn.net/gaokcl/article/details/93541337
    try {
        //设置字符集
        \Workerman\Protocols\Http::header("Content-Type:text/html;charset=UTF-8");
        //开启session
        \Workerman\Protocols\Http::sessionSavePath(RUNTIMEPATH.'session');
        \Workerman\Protocols\Http::sessionStart();
        
        //设置时区
        date_default_timezone_set('Asia/Shanghai');
        require BASEPATH.'init.inc.php';
    } catch (Exception $e) {
        $connection->send($e->getMessage());
    }
};

$http_worker->onError = function ($connection, $code, $msg) {
    echo "error $code $msg\n";
};

// run all workers
Worker::runAll();
