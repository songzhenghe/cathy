<?php /*a:2:{s:46:"/home/szh/cathy/app/views/article/outside.html";i:1607238852;s:43:"/home/szh/cathy/app/views/common/cssjs.html";i:1607238852;}*/ ?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo htmlentities($SETTING['website_name']); ?></title>
    <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/static/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/static/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="/static/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="/static/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/static/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/static/adminlte/dist/js/adminlte.min.js"></script>

  
<script type="text/javascript" src="/static/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<!-- $( '#editor1' ).ckeditor();$( '#editor1' ).val( $( 'input#val' ).val() ); -->

<script src="/static/WdatePicker/WdatePicker.js"></script>
<script src="/static/js/jquery.cookie.js" ></script>
<script src="/static/colorbox/jquery.colorbox-min.js"></script>
<script src="/static/js/jquery.form.js"></script>
<script src="/static/js/jquery.serializeExtend-1.0.1.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/vue-resource.min.js"></script>
<script src="/static/js/jquery.qrcode.min.js"></script>


</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper"> 

            <!-- Default box -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">文章列表</h3>
                </div>
                <div class="card-body p-0">
                  <form action="" method="get">
                    <input type="hidden" name="r" value="<?php echo htmlentities($_GET['r']); ?>">
                    <input type="hidden" name="callback" value="<?php echo htmlentities($callback); ?>">
                    <table class="table table-bordered">
                      <tr>
                        <td>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">ID</span>
                            </div>
                            <input type="text" name="itemid" class="" placeholder="" value="<?php echo htmlentities($itemid); ?>">
                            &nbsp;
                            <div class="input-group-prepend">
                                <span class="input-group-text">所属分类</span>
                              </div>
                              <?php echo $category; ?>
                              &nbsp;
                            <div class="input-group-prepend">
                              <span class="input-group-text">标题</span>
                            </div>
                            <input type="text" name="title" class="" placeholder="" value="<?php echo htmlentities($title); ?>">
                            &nbsp;
                            <div class="input-group-prepend">
                              <span class="input-group-text">状态</span>
                            </div>
                            <select class="" name="flag">
                                <option value="0">请选择</option>
                                <?php foreach(\core_enums::$flag as $k=>$v): ?>
                                <option value="<?php echo htmlentities($k); ?>" <?php if($flag==$k): ?>selected<?php endif; ?>><?php echo htmlentities($v); ?></option>
                                <?php endforeach; ?>
                            </select>
                            &nbsp;
                            
                            <button name="submit" type="submit" class="btn btn-primary">搜索</button>
                            &nbsp;
                            <button type="button" class="btn btn-warning" onclick="location.href='?r=article/outside&callback=<?php echo htmlentities($callback); ?>';">重置</button>
                          </div>
      
                        </td>
                      </tr>
                    </table>
                  </form>
                  
                  <table class="table table-bordered">
                    <?php if(count($data)): ?>
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>所属分类</th>
                        <th>标题</th>
                        <th>添加人</th>
                        <th>添加时间</th>
                        <th>显隐</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data as $k=>$v): ?>
                      <tr>
                        <td><?php echo htmlentities($v['itemid']); ?></td>
                        <td><?php echo $v['cat']['cat_pos']; ?></td>
                        <td><a href='javascript:my_click(<?php echo json_encode($v); ?>);'><?php echo htmlentities($v['title']); ?></a></td>
                        <td><?php echo htmlentities($v['username']); ?></td>
                        <td><?php echo htmlentities($v['addtime']); ?></td>
                        <td><?php echo myswitch($v['flag'],core_enums::$flag); ?></td>
                      </tr>
                      <?php endforeach; ?>
                      <tr>
                        <td colspan="100">
                          <?php echo $pager; ?>
                        </td>
                      </tr>
                      <?php else: ?>
                      <tr>
                        <td colspan="100">暂无数据</td>
                      </tr>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->


  </div>
  <script>
  function my_click(arr){
    parent.<?php echo htmlentities($callback); ?>(arr);	
  }
  </script>
</body>

</html>