<?php /*a:2:{s:42:"/home/szh/cathy/app/views/upload/huge.html";i:1581513007;s:43:"/home/szh/cathy/app/views/common/cssjs.html";i:1581478438;}*/ ?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php echo htmlentities($SETTING['website_name']); ?></title>

    <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/static/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/static/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="/static/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="/static/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/static/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/static/adminlte/dist/js/adminlte.min.js"></script>

  
<script type="text/javascript" src="/static/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<!-- $( '#editor1' ).ckeditor();$( '#editor1' ).val( $( 'input#val' ).val() ); -->

<script src="/static/WdatePicker/WdatePicker.js"></script>
<script src="/static/js/jquery.cookie.js" ></script>
<script src="/static/colorbox/jquery.colorbox-min.js"></script>
<script src="/static/js/jquery.form.js"></script>
<script src="/static/js/jquery.serializeExtend-1.0.1.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/vue-resource.min.js"></script>
<script src="/static/js/jquery.qrcode.min.js"></script>


</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <h3 style="text-align: center;">文件类型：jpg,png,gif,rar,zip,pdf,txt,doc,docx,ppt,pptx,xls,xlsx,swf,iso最大2GB</h3>
          
    <div style="padding:20px;text-align: center;">
      <input type="file" name="" id="file">
      <button id="upload">上传</button>
      <span id="output" style="font-size:12px">等待</span>
    </div>


  </div>

  

<script>

  $('#upload').click(function(){
    if(!$("#file")[0].files[0]){
      alert('请先选择文件');
      return;	
    }
    var file = $("#file")[0].files[0],  //文件对象
      name = file.name,        //文件名
      size = file.size,        //总大小
      succeed = 0;
    var shardSize = 2 * 1024 * 1024,    //以2MB为一个分片
      shardCount = Math.ceil(size / shardSize);  //总片数
      
      
    window.start=function(i){
      //计算每一片的起始与结束位置
      var start = i * shardSize,
        end = Math.min(size, start + shardSize);
      //构造一个表单，FormData是HTML5新增的
      var form = new FormData();
      form.append("data", file.slice(start,end));  //slice方法用于切出文件的一部分
      form.append("name", name);
      form.append("total", shardCount);  //总片数
      form.append("index", i + 1);        //当前是第几片
      form.append("fileSize", file.size); //文件大小
      form.append("class", '<?php echo htmlentities($class); ?>');
      form.append("i", '<?php echo htmlentities($i); ?>');
      form.append("callback", '<?php echo htmlentities($callback); ?>');
      form.append('uniq', '<?php echo htmlentities($uniq); ?>');
      //Ajax提交
      $.ajax({
        url: "<?php echo htmlentities($UPLOAD_HUGE); ?>",
        type: "POST",
        data: form,
        async: true,        //异步
        processData: false,  //很重要，告诉jquery不要对form进行处理
        contentType: false,  //很重要，指定为false才能形成正确的Content-Type
        dataType: 'json',
        success: function(data){
          if(parseInt(data.result.code)!=0){
            alert(data.result.msg);
            return;	
          }else{
            if(data.result.url){
              alert('上传成功');
              if(typeof(window.parent.<?php echo htmlentities($callback); ?>)!='undefined'){
                window.parent.<?php echo htmlentities($callback); ?>(data.result);
              }else{
                console.log(data);
              }
              return;
            }
            
          }
          ++succeed;
          $("#output").text(succeed + " / " + shardCount + " [" + Math.round(succeed/shardCount*100) + "%]");
          ++i;
          if(i < shardCount) window.start(i);
        },
        error: function(){
          alert('上传失败');	
        }
      });
    }
    
    start(0);	
  });
  
</script>  
</body>

</html>