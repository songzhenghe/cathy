<?php /*a:2:{s:56:"/home/szh/cathy/app/views/category/category_outside.html";i:1581513008;s:43:"/home/szh/cathy/app/views/common/cssjs.html";i:1581478438;}*/ ?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php echo htmlentities($SETTING['website_name']); ?></title>

    <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/static/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/static/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="/static/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="/static/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/static/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/static/adminlte/dist/js/adminlte.min.js"></script>

  
<script type="text/javascript" src="/static/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<!-- $( '#editor1' ).ckeditor();$( '#editor1' ).val( $( 'input#val' ).val() ); -->

<script src="/static/WdatePicker/WdatePicker.js"></script>
<script src="/static/js/jquery.cookie.js" ></script>
<script src="/static/colorbox/jquery.colorbox-min.js"></script>
<script src="/static/js/jquery.form.js"></script>
<script src="/static/js/jquery.serializeExtend-1.0.1.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/vue-resource.min.js"></script>
<script src="/static/js/jquery.qrcode.min.js"></script>


</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">

      <table class="table table-bordered table-hover" width="100%">
                    
          <tr>
            <td colspan="7" align="center">
            <p class="mysearch_item">搜索：<input type="text" name="" id="searching"> </p>
            <p class="mysearch_item2"><input class="btn btn-primary" type="button" value="搜索" id="to_search"> <input type="button" class="btn btn-warning" value="重置" id="to_reset"></p>
            </td>
          </tr>
          
          </table>
          <div class="box-body table-responsive no-padding">
          <table class="table table-bordered table-hover mytable" width="100%">
          <tr>
            <td>名称</td>
            <td align="center">分类ID</td>
            <td align="center">路径</td>
          </tr>
          <?php foreach($category_arr as $key => $value): ?>
          <tr>
            <td>
            <?php echo str_repeat('&nbsp;',$value['lev']*4); ?>┖─&nbsp;<strong><a href="javascript:my_click(['<?php echo htmlentities($value['catid']); ?>','<?php echo dhtmlspecialchars($value['catname']); ?>','<?php echo dhtmlspecialchars($value['cat_pos']); ?>']);"><?php echo htmlentities($value['catname']); ?></a></strong>
            </td>
          <td align="center"><?php echo htmlentities($value['catid']); ?></td>
          <td align="center"><?php echo $value['cat_pos']; ?></td>
          </tr>
          <?php endforeach; ?>
          </table>    
    </div>


  </div>
  <script>
      $(document).ready(function(e) {
          $('#to_search').click(function(){
          var txt=$('#searching').val();
          if(txt===''){
            $('#to_reset').click();	
          }
          $('.mytable:eq(0) tr').each(function(index, element) {
                  var a=$(element).find('td').eq(0).text();
            if(a.indexOf(txt)>-1){
              $(element).show();	
            }else{
              $(element).hide();
            }
              });
          
        });
        $('#to_reset').click(function(){
          $('.mytable:eq(0) tr').show();
          $('#searching').val('');
        });
      });
      function my_click(arr){
        parent.<?php echo $callback;?>(arr);	
      }
      </script>
</body>

</html>