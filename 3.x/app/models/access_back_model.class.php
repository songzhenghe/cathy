<?php
class access_back_model extends common_model{
    
    //后台权限
	public function set_auth($userid,$post){
		isset($post['access']) or $post['access']=[];
		$this->delete(['userid'=>$userid,'moduleid'=>$post['moduleid']]);
		
		foreach($post['access'] as $controller=>$actions){
			foreach($actions as $action){
                $this->insert(['userid'=>$userid,'moduleid'=>$post['moduleid'],'module'=>$post['module'],'controller'=>$controller,'action'=>$action]);
			}
		}
		return true;
	}
	public function get_auth($userid,$moduleid){
		$data=$this->select('*',['userid'=>$userid,'moduleid'=>$moduleid]);
		$return=[];
		foreach($data as $k=>$v){
			$return[$v['controller']][]=$v['action'];
		}
		return $return;
	}

}