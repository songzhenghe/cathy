<?php
class article_model extends common_model{
    
    //处理编辑时的垃圾附件
    public function treat_attachment($pk){
        $data=$this->find('*',['itemid'=>$pk]);
        $string=$data['thumb'].$data['content'];
        
        $model=model('article_attachment');
        $attachments=$model->select('*',['articleid'=>$pk]);
        foreach($attachments as $k=>$v){
            if(strpos($string,$v['url'])!==false) continue;

            $file=ROOTPATH.'public'.$v['url'];
            if(!file_exists($file)) continue;
            unlink($file);
            $model->delete(['itemid'=>$v['itemid']]);
        }
        return true;
    }
    
}