<?php
class category_model extends common_model{
    protected $categorys = [];

    public function check_catid($catid,$module_category){
		if($module_category=='*') return true;
		return in_array($catid,$module_category);
    }
    
    public function getCatTree($arr,$id=0,$lev=0){
	    $tree = array();
	    foreach($arr as $v) {
	        if($v['parentid'] == $id) {
	            $v['lev'] = $lev;
	            $tree[] = $v;
	            $tree = array_merge($tree,$this->getCatTree($arr,$v['catid'],$lev+1));
	        }
	    }
	    return $tree;
    }
    
    public function category_select($moduleid,$name,$catid=0,$label='请选择',$extra='',$auth=null){
        $all_cats=$this->db->select('*')->from('szh_category')->where("moduleid='{$moduleid}'")->orderByAsc(['listorder'])->query();
		$category_arr=$this->getCatTree($all_cats);
		$str="<select name=\"{$name}\" {$extra}>";
		$str.="<option value=\"0\">{$label}</option>";
		foreach($category_arr as $k=>$v){
			if($auth!==null and !$this->check_catid($v['catid'],$auth)) continue;
			if($v['catid']==$catid){
				$sel="selected=\"selected\"";
			}else{
				$sel='';
			}
			$str.="<option value=\"{$v['catid']}\" {$sel}>".str_repeat('&nbsp;',$v['lev']*4)."┖─&nbsp;{$v['catname']}</option>";
		}
		$str.="</select>";
		return $str;
    }

    protected function get_arrparentid($catid, $arrparentid = '', $n = 1) {
		if($n > 10 || !is_array($this->categorys) || !isset($this->categorys[$catid])) return false;
		$parentid = $this->categorys[$catid]['parentid'];
		$arrparentid = $arrparentid ? $parentid.','.$arrparentid : $parentid;
		if($parentid) {
			$arrparentid = $this->get_arrparentid($parentid, $arrparentid, ++$n);
		} else {
			$this->categorys[$catid]['arrparentid'] = $arrparentid;
		}
		$parentid = $this->categorys[$catid]['parentid'];
		return $arrparentid;
    }
    
	protected function get_arrchildid($catid) {
		$arrchildid = $catid;
		if(is_array($this->categorys)) {
			foreach($this->categorys as $id => $cat) {
				if($cat['parentid'] && $id != $catid && $cat['parentid']==$catid) {
					$arrchildid .= ','.$this->get_arrchildid($id);
				}
			}
		}
		return $arrchildid;
    }
    
	public function cache($moduleid){
        $categorys=$this->db->select('*')->from('szh_category')->where("moduleid='{$moduleid}'")->orderByAsc(['listorder'])->query();
		$this->categorys=\ka_builder($categorys,'catid');
		foreach($this->categorys as $catid => $cat) {
			$arrparentid = $this->get_arrparentid($catid);
			$arrchildid = $this->get_arrchildid($catid);
			$child = is_numeric($arrchildid) ? 0 : 1;
			$tmp=array();
			$tmp=array_merge($tmp,$cat);
			$tmp['arrparentid']=$arrparentid;
			$tmp['arrchildid']=$arrchildid;
			$tmp['child']=$child;
			$tmp['cat_pos']=$this->get_cat_pos($tmp);
			
            $this->update([
                'arrparentid'=>$tmp['arrparentid'],
                'arrchildid'=>$tmp['arrchildid'],
                'child'=>$tmp['child'],
                'cat_pos'=>$tmp['cat_pos'],
            ],['catid'=>$catid]);
		}

		return true;
	}

	public function get_category($catid){
		$catid=intval($catid);
		return $this->find('*',['catid'=>$catid]);
	}
	
	public function get_cat_pos($CAT) {
		$str = ' &raquo; ';
		$target='';
		if(!$CAT) return '';
		
		if(!isset($CAT['arrparentid']) || !$CAT['arrparentid']){
			return $CAT['catname'];
		}
		
		$arrparentids = $CAT['arrparentid'].','.$CAT['catid'];
		$arrparentid = explode(',', $arrparentids);
		$pos = '';
		$target = $target ? ' target="_blank"' : '';	
		$CATEGORY = array();
        $ddd = $this->select('*',['catid'=>[$arrparentids,'in']]);
		
		foreach($ddd as $r){
			$CATEGORY[$r['catid']] = $r;
		}
		$last=count($arrparentid)-1;
		foreach($arrparentid as $index=>$catid) {
			if(!$catid || !isset($CATEGORY[$catid])) continue;
			if($index==$last){
				$pos .= $CATEGORY[$catid]['catname'].$str;
			}else{
				//$pos .= '<a href="?r='.$_GET['r'].'&parentid='.$catid.'"'.'"'.$target.'>'.$CATEGORY[$catid]['catname'].'</a>'.$str;
				$pos .= $CATEGORY[$catid]['catname'].$str;
			
			}
			
		}
		$_len = strlen($str);
		if($str && substr($pos, -$_len, $_len) === $str) $pos = substr($pos, 0, strlen($pos)-$_len);
		return $pos;
    }
    
	public function max_catid(){
        return $this->value('max(catid) as m');
	}
	
	public function mod_check_ok($moduleid,$catid,$parentid){
        $all_cats=$this->db->select('*')->from('szh_category')->where("moduleid='{$moduleid}'")->orderByAsc(['listorder'])->query();
		$sub_cats = $this->getCatTree($all_cats,$catid);
		$ids = array();
		foreach ($sub_cats as $v) {
			$ids[] = $v['catid'];
		}
		#判断所选的父分类是否为当前分类或其后代分类
		if ($parentid == $catid || in_array($parentid, $ids)) return false;
		return true;
    }
    
	public function del_check_ok($catid,$moduleid){
        $r=$this->find('catid',['parentid'=>$catid]);
		if($r) return false;
        
        $core_conf=new \core_conf();
        $M=$core_conf->mod_single($moduleid);
		$table=$M['module_en'];
        $model=model(strtolower($table));
		$r=$this->value('count(*) as c',['catid'=>$catid]);
		if($r) return false;
		return true;
    }
}