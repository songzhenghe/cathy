<?php
class core_enums
{
    public static $flag = array(
        1 => '显示',
        2 => '隐藏',
        3 => '删除',
        //0=>'----',
    );
    public static $member_group = array(
        1=>'超级管理员',
        2=>'普通管理员',
        3=>'禁止访问',
    );
}