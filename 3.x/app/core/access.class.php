<?php
class access extends controller{
	public $userid;
	public $username;

	protected $auth;
	protected $moduleid=0;
	public function __construct($prop){
		parent::__construct($prop);
	}

	function _before(){
		parent::_before();
		//检查是否登录
		if(!isset($_SESSION['ADMIN'])){
			$this->CONN->close(msg(0,'非法操作','welcome/login'));
			return false;
		}
		$this->userid=$_SESSION['ADMIN']['userid'];
		$this->username=$_SESSION['ADMIN']['username'];
		$this->auth=$_SESSION['ADMIN'];

		//全局模板变量
		$this->getView()->assign(['ADMIN'=>$this->auth]);
		
		//check module is exist
        if(in_array($this->MM,['frameset','upload','common'])){
            return true;
        }
        if(!isset($this->CORE_CONF->MAP[$this->MM])){
            $this->msg(0,'模块不存在');
            return false;
        }
        $this->moduleid=$moduleid=$this->CORE_CONF->MAP[$this->MM];
        //check rule is exist
        if(!$this->CORE_CONF->rule_exists($moduleid,$this->AA)){
            $this->msg(0,$this->MM.':'.$this->AA.'权限规则不存在');
            return false;
        }
        if($this->auth['groupid']==1){
            return true;
        }
        if(!$this->CORE_CONF->MODULE[$moduleid]['is_auth']){
            return true;
        }
        if(!$this->CORE_CONF->AUTH[$moduleid][$this->AA]['is_auth']){
            return true;
        }
        //check if has right
        $model=model('access_back');
        $d=$model->find('itemid',['userid'=>$this->auth['userid'],'moduleid'=>$moduleid,'module'=>$this->MM,'controller'=>$this->CC,'action'=>$this->AA]);
        if(!$d){
            $this->msg(0,'没有权限');
            return false;
        }

		return true;
	}
	function _after(){

	}


}