<?php
//系统配置类
class core_conf{
    public $MODULE=array();
    public $MODULE_BYNAME=array();
    public $MAP=array();
    public $AUTH=array();
    public $MENU=array();

    public function __construct(){
        $this->module();
        $this->auth();
        $this->menu();
    }
    private function module(){
        $MODULE=array(
            array('moduleid'=>1,'module_en'=>'admin','module_cn'=>'网站设置','module_dir'=>'admin','is_auth'=>1),
            array('moduleid'=>2,'module_en'=>'member','module_cn'=>'会员管理','module_dir'=>'member','is_auth'=>1),
            array('moduleid'=>11,'module_en'=>'article','module_cn'=>'文章管理','module_dir'=>'article','is_auth'=>1),
			
        );
        foreach($MODULE as $k=>$v){
            $this->MODULE[$v['moduleid']]=$v;
            $this->MODULE_BYNAME[$v['module_en']]=$v;
            $this->MAP[$v['module_en']]=$v['moduleid'];
        }
    }
    
    private function auth(){
        $AUTH=array(
            1=>[
                array('name'=>'safe','chinese'=>'修改密码','is_auth'=>0,'note'=>'','is_menu'=>1),
				array('name'=>'setting','chinese'=>'网站设置','is_auth'=>1,'note'=>'','is_menu'=>1),
            ],

            2=>[
                array('name'=>'add','chinese'=>'添加会员','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'lst','chinese'=>'会员列表','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'mod','chinese'=>'编辑会员','is_auth'=>1,'note'=>'','is_menu'=>0),
				array('name'=>'del','chinese'=>'删除会员','is_auth'=>1,'note'=>'','is_menu'=>0),
				array('name'=>'login_log','chinese'=>'登录日志','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'setting','chinese'=>'模块设置','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'authenticate','chinese'=>'后台权限','is_auth'=>1,'note'=>'','is_menu'=>0),
                array('name'=>'outside','chinese'=>'外部调用','is_auth'=>0,'note'=>'','is_menu'=>0),
            ],
            11=>[
                array('name'=>'add','chinese'=>'添加文章','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'lst','chinese'=>'文章列表','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'mod','chinese'=>'编辑文章','is_auth'=>1,'note'=>'','is_menu'=>0),
				array('name'=>'del','chinese'=>'删除文章','is_auth'=>1,'note'=>'','is_menu'=>0),
				array('name'=>'preview','chinese'=>'查看文章','is_auth'=>1,'note'=>'','is_menu'=>0),
				array('name'=>'setting','chinese'=>'模块设置','is_auth'=>1,'note'=>'','is_menu'=>1),
                array('name'=>'outside','chinese'=>'外部调用','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'clear_attachments','chinese'=>'清理垃圾附件','is_auth'=>1,'note'=>'','is_menu'=>1),
                array('name'=>'test','chinese'=>'测试','is_auth'=>0,'note'=>'','is_menu'=>1),
                
                array('name'=>'category_add','chinese'=>'添加分类','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'category_mod','chinese'=>'编辑分类','is_auth'=>1,'note'=>'','is_menu'=>0),
				array('name'=>'category_lst','chinese'=>'分类列表','is_auth'=>1,'note'=>'','is_menu'=>1),
				array('name'=>'category_del','chinese'=>'删除分类','is_auth'=>1,'note'=>'','is_menu'=>0),
				array('name'=>'category_cache','chinese'=>'更新分类缓存','is_auth'=>0,'note'=>'','is_menu'=>1),
                array('name'=>'category_outside','chinese'=>'外部调用','is_auth'=>0,'note'=>'','is_menu'=>0),
                
                array('name'=>'upload_img','chinese'=>'图片上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_img_action','chinese'=>'图片上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_file','chinese'=>'文件上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_file_action','chinese'=>'文件上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_ck_img','chinese'=>'ck图片上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_ck_file','chinese'=>'ck附件上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_huge','chinese'=>'分段上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_huge_action','chinese'=>'分段上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_img_tradition','chinese'=>'ajax图片上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_img_tradition_action','chinese'=>'ajax图片上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_file_tradition','chinese'=>'ajax文件上传','is_auth'=>0,'note'=>'','is_menu'=>0),
                array('name'=>'upload_file_tradition_action','chinese'=>'ajax文件上传','is_auth'=>0,'note'=>'','is_menu'=>0),
    
            ],
        );
        $t=[];
        foreach($AUTH as $k=>$v){
            $t[$k]=[];
            foreach($v as $vv){
                $t[$k][$vv['name']]=$vv;
            }
        }
        $this->AUTH=$t;
    }
    private function menu(){
        //附加菜单
        $MENU=array(
            1=>[
                
            ],

            2=>[
                
            ],

            11=>[
                
            ],
        );
        $t=[];
        foreach($MENU as $k=>$v){
            $t[$k]=[];
            foreach($v as $vv){
                $t[$k][$vv['name']]=$vv;
            }
        }
        $MENU=$t;

        $t=[];
        foreach($this->AUTH as $k=>$v){
            $t[$k]=[];
            foreach($v as $vv){
                if(!$vv['is_menu']) continue;
                $t[$k][$vv['name']]=$vv;
            }
            $t[$k]=array_merge($t[$k],(array) $MENU[$k]);
        }
        $this->MENU=$t;
    }
    public function mod_exists($moduleid){
        return array_key_exists($moduleid,$this->MODULE);
    }
    public function mod_single($moduleid){
        return $this->MODULE[$moduleid];
    }
    public function mod_auth($moduleid){
        return $this->AUTH[$moduleid];
    }
    public function mod_menu($moduleid){
        return $this->MENU[$moduleid];
    }
    public function rule_exists($moduleid,$k){
        return array_key_exists($k,$this->AUTH[$moduleid]);
    }
    public function rule_single($moduleid,$k){
        return $this->AUTH[$moduleid][$k];
    }
    public function user_menu($groupid,$userid,$menu){
        if($groupid==1) return $menu;
        $t=[];
		$model=model('access_back');
        foreach($menu as $k=>$v){
            $t[$k]=[];
            $RIGHT=$model->select('*',['userid'=>$userid,'moduleid'=>$k]);
            $RIGHT=\kc_builder($RIGHT,'action');
            foreach($v as $vv){
                if($vv['is_auth'] && !in_array($vv['name'],$RIGHT)) continue;
                $t[$k][$vv['name']]=$vv;
            }
        }
        return $t;
    }
    
}