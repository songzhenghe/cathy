<?php
class admin extends access{

    public function __construct($prop){
		parent::__construct($prop);
        
	}

    public function _before(){
        if(!parent::_before()) return false;

        $this->moduleid=1;
        $this->MD=$this->CORE_CONF->mod_single($this->moduleid);
		copy_setting($this->moduleid);
        $this->setting=load_setting($this->moduleid);
        $this->getView()->assign(['MODULEID'=>$this->moduleid,'MD'=>$this->MD]);
    }

    public function safe(){
        $post = $_POST;
        if($post){
            $old_password=trim($post['old_password']);
            $new_password=trim($post['new_password']);
            $new_password2=trim($post['new_password2']);
            if(!$old_password or !$new_password or !$new_password2){
                $this->msg(0,'请填写完密码后再提交');
                return;
            }
            if($new_password!=$new_password2){
                $this->msg(0,'两次新密码不一致');
                return;
            }
            $userid=$this->auth['userid'];
            $model = model('member');
            $user = $model->find('*',"userid='{$userid}'");
            if(md5($old_password)!=$user['password']){
                $this->msg(0,'旧密码不正确');
                return;
            }

            $affected_rows=$model->update(['password'=>md5($new_password)],"userid='{$userid}'");

            $this->msg($affected_rows);
            return;
        }

        return $this->tpl(get_defined_vars());
    }

    public function setting(){
        $post = $_POST;
        if($post){
            $r=save_setting($this->moduleid,$post);
            $this->msg($r);
            return;
        }
        $setting=load_setting($this->moduleid);
        return $this->tpl(get_defined_vars());
    }


}