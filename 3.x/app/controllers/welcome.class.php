<?php
class welcome extends controller {
	public function __construct($prop){
		parent::__construct($prop);
		$this->model=model('member');
	}
	public function index(){
		return $this->jump('welcome/login');
	}

	public function login(){
		if(isset($_SESSION['ADMIN'])){
			return $this->jump('frameset/index');
		}
		$post = $_POST;
        if($post){
            $username=trim($post['username']);
            $password=trim($post['password']);
            $code=trim($post['code']);
            if(!$username or !$password){
                $this->msg(0,'账号和密码不能为空');
                return;
            }
            if(!$code){
                $this->msg(0,'验证码不能为空');
                return;
            }
            if(strtolower($code)!=strtolower($_SESSION['CODE'])){
                $this->msg(0,'验证码不正确');
                return;
            }
            $user=$this->model->find('*',"username='{$username}'");
            if(!$user){
                $this->msg(0,'账号不存在');
                return;
            }
            if($user['groupid']!=1 and $user['groupid']!=2){
                $this->msg(0,'账号已经被禁用');
                return;
            }
            //密码 123456
            if(md5($password)!=$user['password']){
                $this->msg(0,'密码错误');
                return;
            }
            $login_back_model=model('login_back');
            $login_back_model->insert(['username'=>$username,'loginip'=>$this->realIP,'logintime'=>time(),'agent'=>$_SERVER['HTTP_USER_AGENT']]);

            unset($user['password']);
            //session
			$_SESSION['ADMIN']=$user;
            $this->msg(1,'登录成功');
            return;
		}
		return $this->tpl(get_defined_vars());
    }
    
	
	
}