<?php
class frameset extends access{
    public function __construct($prop){
		parent::__construct($prop);
	}
    public function index(){
        $model=model('member');
        $data=$model->query("select VERSION() as version");
        $mysql_version=$data[0]['version'];
        return $this->tpl(get_defined_vars());
    }

    public function logout(){
        unset($_SESSION['ADMIN']);
        \Workerman\Protocols\Http::setcookie('PHPSESSID',null,time()-3600);
        return $this->jump('welcome/login');
    }
    public function set_menu_current(){
        $post = $_POST;
        $_SESSION['open_sidebar_menu']=$post['a'];
        $_SESSION['open_sidebar_item']=$post['href'];

        return json_encode([
            'result'=>[
                time()
            ]
        ]);
    }
    public function get_menu_current(){
        $a=isset($_SESSION['open_sidebar_menu'])?$_SESSION['open_sidebar_menu']:0;
        $href=isset($_SESSION['open_sidebar_item'])?$_SESSION['open_sidebar_item']:'#';
        return json_encode([
            'result'=>['a'=>$a,'href'=>$href]
        ]);
    }
}