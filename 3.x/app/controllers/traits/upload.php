<?php
Trait upload{

    protected function upload_file_common($extensions){
        $post=$_POST;
        if($post){
            $class=$_REQUEST['class'];
            $i=$_REQUEST['i'];
            $callback=$_REQUEST['callback'];
            $uniq=$_REQUEST['uniq'];
            //file_put_contents('a.php',var_export($_FILES,true));
            $config=array(
				'field_name'=>0,
				'upload_path'=>PUBLICPATH.'uploads/',
				'allow_type'=>$extensions,//数组
				'max_size'=>'2',
				'unit'=>'M',
				'israndname'=>true,
				'iscreatedir'=>true,
				'iscreatebydate'=>true,
				'isoverride'=>true
			);
			$upload=new sys_upload();
			$r=$upload->upload_instance($config);

            if($r['error_number']<0){
                return json_encode([
                    'result'=>['code'=>1,'msg'=>$r['error_message'],'url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
                ]);
            }
            $url='/uploads/'.$r['file_relpath'].$r['new_name'];
            //save to db
            if(method_exists($this,'save_attachment')){
                $this->save_attachment(0,$uniq,$url);
            }
            
            return json_encode([
                'result'=>['code'=>0,'msg'=>'success','url'=>$url,'class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
    }
    public function upload_img(){
        $get=$_GET;
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            $this->msg(0,'lose class/i/callback/uniq');
            return;
        }
        return $this->tpl(get_defined_vars(),'upload/img');
    }
    public function upload_img_action(){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
        ];
        return $this->upload_file_common($extensions);
    }
    public function upload_file(){
        $get=$_GET;
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            $this->msg(0,'lose class/i/callback/uniq');
            return;
        }
        return $this->tpl(get_defined_vars(),'upload/file');
    }
    public function upload_file_action(){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
            '.rar',
            '.zip',
            '.pdf',
            '.txt',
            '.doc',
            '.docx',
            '.ppt',
            '.pptx',
            '.xls',
            '.xlsx',
            '.swf',
        ];
        return $this->upload_file_common($extensions);
    }

    private function mkhtml($fn,$fileurl,$message)
	{
		$str='<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$fn.', \''.$fileurl.'\', \''.$message.'\');</script>';
		$this->CONN->send($str);
    }
    
    protected function ck_common($extensions){
        $post=$_POST;
        if(!$post){
            $this->msg(0);
            return;
        }
        $get=$_GET;
        extract($get);
        if(empty($CKEditorFuncNum)){
            $this->msg(0,'错误的功能调用请求！');
            return;
        }
        if(!isset($uniq)){
            $this->mkhtml(1,'','lose uniq!');
            return;
        }

        $config=array(
            'field_name'=>0,
            'upload_path'=>PUBLICPATH.'uploads/',
            'allow_type'=>$extensions,//数组
            'max_size'=>'2',
            'unit'=>'M',
            'israndname'=>true,
            'iscreatedir'=>true,
            'iscreatebydate'=>true,
            'isoverride'=>true
        );
        $upload=new sys_upload();
        $r=$upload->upload_instance($config);

        if($r['error_number']<0){
            $this->mkhtml($CKEditorFuncNum,'',$r['error_message']);
            return;
        }
        $url='/uploads/'.$r['file_relpath'].$r['new_name'];
        //save to db
        if(method_exists($this,'save_attachment')){
            $this->save_attachment(0,$uniq,$url);
        }
        $this->mkhtml($CKEditorFuncNum,$url,'上传成功！');
        return;
    }

    public function upload_ck_img(){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
        ];
        return $this->ck_common($extensions);
    }

    public function upload_ck_file(){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
            '.rar',
            '.zip',
            '.pdf',
            '.txt',
            '.doc',
            '.docx',
            '.ppt',
            '.pptx',
            '.xls',
            '.xlsx',
            '.swf',
        ];
        return $this->ck_common($extensions);
    }

    public function upload_huge(){
        $get=$_GET;
        extract($get);
        
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            $this->msg(0,'lose class/i/callback/uniq');
            return;
        }
        return $this->tpl(get_defined_vars(),'upload/huge');
    }

    public function upload_huge_action(){
        //http://doc.workerman.net/web-server.html
        //https://blog.csdn.net/oljuydfcg/article/details/90605499
/*
array (
  0 => 
  array (
    'name' => 'data',
    'file_name' => 'blob',
    'file_data' => '',
    'file_size' => 2097152,
    'file_type' => 'application/octet-stream',
  ),
)
*/
        $name=$_REQUEST['name'];
        $total=$_REQUEST['total'];
        $index=$_REQUEST['index'];
        $fileSize=$_REQUEST['fileSize'];

        $class=$_REQUEST['class'];
        $i=$_REQUEST['i'];
        $callback=$_REQUEST['callback'];
        $uniq=$_REQUEST['uniq'];

        $file=current($_FILES);
        //file_put_contents('a.php',var_export($_FILES,true));
        //主要判断文件大小和类型
        if($fileSize>2*1024*1024*1024){
            return json_encode([
                'result'=>['code'=>101,'msg'=>'文件超出最大大小','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        $ext=pathinfo($name,PATHINFO_EXTENSION);
        $extensions=[
            'jpg',
            'png',
            'gif',
            'rar',
            'zip',
            'pdf',
            'txt',
            'doc',
            'docx',
            'ppt',
            'pptx',
            'xls',
            'xlsx',
            'swf',
			'iso',
        ];
        if(!in_array($ext,$extensions)){
            return json_encode([
                'result'=>['code'=>102,'msg'=>'文件类型不正确','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        $DEST=isset($_SESSION['UPLOAD_HUGE_DEST'])?$_SESSION['UPLOAD_HUGE_DEST']:'';
        if($DEST){
            $destination=$DEST;
        }else{
            mt_srand();
            $destination=PUBLICPATH.'uploads/'.date('Y/m/d').'/'.time().rand(1000,9999).'.'.$ext;    
            $_SESSION['UPLOAD_HUGE_DEST']=$destination;
        }
        
        if(!file_exists(dirname($destination))){
            mkdir(dirname($destination),0777,true);
        }
        $url=str_replace(ROOTPATH.'public','',$destination);

        //如果第一次上传的时候，该文件已经存在，则删除文件重新上传
        if($index==1 && file_exists($destination) && filesize($destination)==$fileSize){
            unlink($destination);
        }
        //合并文件
        $r=file_put_contents($destination, $file['file_data'], FILE_APPEND);
        if(!$r){
            return json_encode([
                'result'=>['code'=>103,'msg'=>'文件写入失败','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        //最后一块时
        if($index==$total){
            //save to db
            if(method_exists($this,'save_attachment')){
                $this->save_attachment(0,$uniq,$url);
            }
            unset($_SESSION['UPLOAD_HUGE_DEST']);
            return json_encode([
                'result'=>['code'=>0,'msg'=>'success','url'=>$url,'class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        //继续上传
        return json_encode([
            'result'=>['code'=>0,'msg'=>'success','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
        ]);
    }
    public function upload_img_tradition(){
        $get=$_GET;
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            $this->msg(0,'lose class/i/callback/uniq');
            return;
        }
        return $this->tpl(get_defined_vars(),'upload/img_tradition');
    }
    public function upload_img_tradition_action(){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
        ];
        return $this->upload_file_common($extensions);
    }
    public function upload_file_tradition(){
        $get=$_GET;
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            $this->msg(0,'lose class/i/callback/uniq');
            return;
        }
        return $this->tpl(get_defined_vars(),'upload/file_tradition');
    }
    public function upload_file_tradition_action(){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
            '.rar',
            '.zip',
            '.pdf',
            '.txt',
            '.doc',
            '.docx',
            '.ppt',
            '.pptx',
            '.xls',
            '.xlsx',
            '.swf',
        ];
        return $this->upload_file_common($extensions);
    }



}