<?php
trait category{

    public function category_add(){
        
        $post=$_POST;
        $model=model('category');
        if($post){
            $post=\dtrim($post);
            $post['moduleid']=$this->moduleid;
            $post['listorder']=intval($post['listorder']);

            
            if(!$post['moduleid']){
                $this->msg(0,'mouleid不能为空');
                return;
            }

            if(!$post['catname']){
                $this->msg(0,'分类名称不能为空');
                return;
            }
            
            $e=$model->find('catid',['moduleid'=>$post['moduleid'],'parentid'=>$post['parentid'],'catname'=>$post['catname']]);
            if($e){
                $this->msg(0,'分类名称已经存在');
                return;
            }

            $insertid=$model->insert($post);
            $model->cache($this->moduleid);
            $this->msg($insertid);
            return;
        }
        $parent=$model->category_select($this->moduleid,'parentid',0,'顶级分类',' class="form-control" ');
        $max_catid=$model->max_catid()+1;
        return $this->tpl(get_defined_vars(),'category/category_add');
    }

    public function category_lst(){
        
        $get=$_GET;
        extract($get);

        $cond=[];
		$cond['moduleid']=$this->moduleid;
        
        $model=model('category');
		$category_arr=$model->select('*',$cond);
		$category_arr=$model->getCatTree($category_arr);

        return $this->tpl(get_defined_vars(),'category/category_lst');
    }

    public function category_mod(){
        
        $get=$_GET;
        extract($get);
        $catid=intval($catid);
		if(!$catid){
            $this->msg(0,'lose catid');
            return;
        }

        $raw=$post=$_POST;
        $model=model('category');
        if($post){
            unset($post['submit'],$post['FORWARD']);
            $post=\dtrim($post);
            $post['moduleid']=$this->moduleid;
            $post['listorder']=intval($post['listorder']);

            if(!$post['moduleid']){
                $this->msg(0,'mouleid不能为空');
                return;
            }

            if(!$post['catname']){
                $this->msg(0,'分类名称不能为空');
                return;
            }
            
            $e=$model->find('catid',['catid'=>[$catid,'!='],'moduleid'=>$post['moduleid'],'parentid'=>$post['parentid'],'catname'=>$post['catname']]);
            if($e){
                $this->msg(0,'分类名称已经存在');
                return;
            }


            $r=$model->mod_check_ok($this->moduleid,$catid,$post['parentid']);
			if(!$r){
                $this->msg(0,'上级分类选择错误');
                return;
            }
            $r=$model->update($post,['catid'=>$catid]);
            $model->cache($this->moduleid);
            $this->msg($r,'',$raw['FORWARD']);
            return;
        }
        $data=$model->find('*',['moduleid'=>$this->moduleid,'catid'=>$catid]);
		if(!$data){
            $this->msg(0,'分类不存在');
            return;
        }
        $parent=$model->category_select($this->moduleid,'parentid',$data['parentid'],'顶级分类',' class="form-control" ');
        return $this->tpl(get_defined_vars(),'category/category_mod');
    }

    public function category_del(){
        
        $get=$_GET;
        extract($get);

        $catid=intval($catid);
		if(!$catid){
            $this->msg(0,'lose catid');
            return;
        }
		$model=model('category');
		$r=$model->del_check_ok($catid,$this->moduleid);
        if(!$r){
            $this->msg(0,'该分类无法删除，请先删除其子分类，并删除其下文章');
            return;
        }
        $cat=$model->find('*',['moduleid'=>$this->moduleid,'catid'=>$catid]);
        if(!$cat){
            $this->msg(0,'数据不存在');
            return;
        }
        $r=$model->delete(['moduleid'=>$this->moduleid,'catid'=>$catid]);
		$model->cache($this->moduleid);
		$this->msg($r===false?false:true);
    }

    public function category_cache(){
        $model=model('category');
        $model->cache($this->moduleid);
        $this->msg(1);
    }

    public function category_outside(){
        
        $get=$_GET;
        extract($get);

        if(!isset($callback)){
            $this->msg(0,'lose callback');
            return;
        }

        $cond=[];
		$cond['moduleid']=$this->moduleid;
        
        $model=model('category');
		$category_arr=$model->select('*',$cond);
		$category_arr=$model->getCatTree($category_arr);

        return $this->tpl(get_defined_vars(),'category/category_outside');
    }

}