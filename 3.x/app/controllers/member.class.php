<?php
class member extends access{
    public function __construct($prop){
		parent::__construct($prop);
        
	}

    public function _before(){
        if(!parent::_before()) return false;

        $this->moduleid=2;
        $this->MD=$this->CORE_CONF->mod_single($this->moduleid);
        copy_setting($this->moduleid);
        $this->setting=load_setting($this->moduleid);
        $this->getView()->assign(['MODULEID'=>$this->moduleid,'MD'=>$this->MD]);
    }

    public function add(){
        $post = $_POST;
        if($post){
            $post=dtrim($post);
            $username=$post['username'];
            if(!$username){
                $this->msg(0,'账号不能为空');
                return;
            }
            $password=$post['password'];
            if(!$password){
                $this->msg(0,'密码不能为空');
                return;
            }
            $model = model('member');
            $e = $model->find('userid',"username='{$username}'");
            if($e){
                $this->msg(0,'账号已经存在');
                return;
            }

            $post['password']=md5($post['password']);

            $insertid = $model->insert($post);
            $this->msg($insertid);
            return;
        }
        $groupid=0;
        return $this->tpl(get_defined_vars());
    }

    public function lst(){
        $param = $_GET;
        extract($param);
        $model = model('member');
        $where = [];
        isset($userid) or $userid='';
        if($userid){
            $where['userid']=$userid;
        }
        isset($username) or $username='';
        if($username){
            $where['username']=$username;
        }
        isset($groupid) or $groupid=0;
        if($groupid){
            $where['groupid']=$groupid;
        }
        isset($truename) or $truename='';
        if($truename){
            $where['truename']=["%{$truename}%",'like'];
        }
        $page = $param['page']??1;
        $pagesize = $this->setting['pagesize'];

        $where=$model->where($where,'1=1');
        
        $data = $model->db()->select('*')->from('szh_member')->where($where)->orderByDesc(['userid'])->limit($pagesize)->offset(($page-1)*$pagesize)->query();
        $total = $model->value('count(*) as total',$where);
        $pager=pagination($_SERVER['REQUEST_URI'],$param,$page,$pagesize,$total);
        return $this->tpl(get_defined_vars());
    }

    public function mod(){
        $get=$_GET;
        $userid=intval($get['userid']);
        if(!$userid){
            $this->msg(0);
            return;
        }
        $raw=$post=$_POST;
        if($post){
            unset($post['submit'],$post['FORWARD']);
            if(!$post['password']){
                unset($post['password']);
            }else{
                $post['password']=md5($post['password']);
            }
            $model = model('member');
            $r=$model->update($post,['userid'=>$userid]);
            $this->msg($r,'',$raw['FORWARD']);
            return;
        }
        $model=model('member');
        $data=$model->find('*',['userid'=>$userid]);
        if(!$data){
            $this->msg(0,'数据不存在');
            return;
        }

        return $this->tpl(get_defined_vars());
    }

    public function del(){
        $get=$_GET;
        $userid=intval($get['userid']);
        if(!$userid){
            $this->msg(0);
            return;
        }
        $model = model('member');
        $data = $model->find('*',['userid'=>$userid]);
        if(!$model){
            $this->msg(0,'数据不存在');
            return;
        }
        $r=$model->update(['groupid'=>3],['userid'=>$userid]);
        $this->msg($r);
        return;
    }

    public function outside(){
        $param = $_GET;
        extract($param);
        if(!isset($callback)){
            $this->msg(0,'lose callback');
            return;
        }
        $model = model('member');
        $where = [];
        isset($userid) or $userid='';
        if($userid){
            $where['userid']=$userid;
        }
        isset($username) or $username='';
        if($username){
            $where['username']=$username;
        }
        isset($groupid) or $groupid=0;
        if($groupid){
            $where['groupid']=$groupid;
        }
        isset($truename) or $truename='';
        if($truename){
            $where['truename']=["%{$truename}%",'like'];
        }
        $page = $param['page']??1;
        $pagesize = $this->setting['pagesize'];

        $where=$model->where($where,'1=1');
        
        $data = $model->db()->select('*')->from('szh_member')->where($where)->orderByDesc(['userid'])->limit($pagesize)->offset(($page-1)*$pagesize)->query();
        $total = $model->value('count(*) as total',$where);
        $pager=pagination($_SERVER['REQUEST_URI'],$param,$page,$pagesize,$total);
        return $this->tpl(get_defined_vars());
    }

    public function login_log(){
        $param = $_GET;
        extract($param);
        $model = model('login_back');
    
        $where=[];
        isset($username) or $username='';
        if($username){
            $where['username']=$username;
        }
        isset($loginip) or $loginip='';
        if($loginip){
            $where['loginip']=$loginip;
        }
        isset($logintime_1) or $logintime_1='';
        if($logintime_1){
            $str=strtotime($logintime_1);
            $where[]=[" logintime >= '{$str}' ",'exp'];
        }
        isset($logintime_2) or $logintime_2='';
        if($logintime_2){
            $str=strtotime($logintime_2.' 23:59:59');
            $where[]=[" logintime <= '{$str}' ",'exp'];
        }
        $page = $param['page']??1;
        $pagesize = $this->setting['pagesize'];
        
        $where=$model->where($where,'1=1');
        
        $data = $model->db()->select('*')->from('szh_login_back')->where($where)->orderByDesc(['itemid'])->limit($pagesize)->offset(($page-1)*$pagesize)->query();
        $total = $model->value('count(*) as total',$where);
        $pager=pagination($_SERVER['REQUEST_URI'],$param,$page,$pagesize,$total);
        return $this->tpl(get_defined_vars());
    }

    public function setting(){
        $post = $_POST;
        if($post){
            $r=save_setting($this->moduleid,$post);
            $this->msg($r);
            return;
        }
        $setting=load_setting($this->moduleid);
        return $this->tpl(get_defined_vars());
    }

    public function authenticate(){
        $get=$_GET;
        extract($get);
        isset($userid) or $userid=0;
		$userid=intval($userid);
		if(!$userid){
            $this->msg(0,'lose userid');
            return;
        }
		isset($moduleid) or $moduleid=1;
		$moduleid=intval($moduleid);
		
		$module_list=$this->CORE_CONF->MODULE;
		if(!array_key_exists($moduleid,$module_list)){
            $this->msg(0,'非法操作');
            return;
        }
		$model=model('access_back');
		
		$post=$_POST;
		if($post){
            $r=$model->set_auth($userid,$post);
            $this->msg($r);
            return;
		}
		
		$mod=$this->CORE_CONF->mod_single($moduleid);
		$checked=$model->get_auth($userid,$moduleid);

        $CORE_CONF=$this->CORE_CONF;
        return $this->tpl(get_defined_vars());
    }

}