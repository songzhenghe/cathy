<?php
require_once APPPATH.'controllers/traits/category.php';
require_once APPPATH.'controllers/traits/upload.php';

class article extends access{

	use category;
    use upload;

	public function __construct($prop){
		parent::__construct($prop);
	}

	public function _before(){
        if(!parent::_before()) return false;

        $this->moduleid=11;
        $this->MD=$this->CORE_CONF->mod_single($this->moduleid);
        \copy_setting($this->moduleid);
        $this->setting=\load_setting($this->moduleid);
        $this->UNIQ=\uniq();
        $this->getView()->assign([
            'MODULEID'=>$this->moduleid,
            'MD'=>$this->MD,
            'CATEGORY_CACHE_URL'=>'?r=article/category_cache',
            'CATEGORY_MOD_URL'=>'?r=article/category_mod',
            'CATEGORY_DEL_URL'=>'?r=article/category_del',
            'UPLOAD_URL1'=>'?r=article/upload_img_action',
            'UPLOAD_URL2'=>'?r=article/upload_file_action',
            'UPLOAD_URL3'=>'?r=article/upload_img_tradition_action',
            'UPLOAD_URL4'=>'?r=article/upload_file_tradition_action',
            'UPLOAD_HUGE'=>'?r=article/upload_huge_action',
            'UNIQ'=>$this->UNIQ,
        ]);
    }

	//attachment hook
	protected function save_attachment($articleid,$uniq,$url){
		$model=model('article_attachment');
		$model->insert([
			'articleid'=>$articleid,
			'uniq'=>$uniq,
			'url'=>$url,
			'ts'=>time(),
		]);
	}

	public function add(){
		
		$post=$_POST;
		$model=model('article');
		if($post){
			$post=\dtrim($post);
			$post['addtime']=time();
			$post['edittime']=time();
			$post['userid']=$this->auth['userid'];
			$post['username']=$this->auth['username'];
			$post['editorid']=$this->auth['userid'];
			$post['editor']=$this->auth['username'];

			if(intval($post['catid'])==0){
				$this->msg(0,'所属分类不能为空');
				return;
			}
			
			if(!$post['title']){
				$this->msg(0,'标题不能为空');
				return;
			}
			
			if(!$post['content']){
				$this->msg(0,'内容不能为空');
				return;
			}

			$insertid=$model->insert($post);
			if($insertid){
				$model2=model('article_attachment');
				$model2->update(['articleid'=>$insertid],['uniq'=>$post['UNIQ']]);
			}
			$this->msg($insertid);
			return;
		}
		$category_model=model('category');
		$category=$category_model->category_select($this->moduleid,'catid',0,'请选择',' class="form-control" ');
		$flag=1;
		return $this->tpl(get_defined_vars());
	}

	public function lst(){
		$param=$_GET;
		extract($param);
		$model = model('article');
		$category_model=model('category');
		$where = [];
		$where['flag']=[[1,2],'in'];
		isset($itemid) or $itemid='';
		if($itemid){
			$where['itemid']=$itemid;
		}
		isset($catid) or $catid=0;
		if($catid){
			$where['catid']=$catid;
		}
		isset($title) or $title='';
		if($title){
			$where['title']=["%{$title}%",'like'];
		}
		isset($flag) or $flag=0;
		if($flag){
			$where['flag']=$flag;
		}
		$page = $param['page']??1;
		$pagesize = $this->setting['pagesize'];

		$where=$model->where($where,'1=1');
        
        $data = $model->db()->select('*')->from('szh_article')->where($where)->orderByDesc(['itemid'])->limit($pagesize)->offset(($page-1)*$pagesize)->query();
		foreach($data as $k=>$v){
			$data[$k]['cat']=$category_model->get_category($v['catid']);
			$data[$k]['addtime']=\timeswitch($v['addtime']);
		}
		$total = $model->value('count(*) as total',$where);
        $pager=pagination($_SERVER['REQUEST_URI'],$param,$page,$pagesize,$total);
		$category=$category_model->category_select($this->moduleid,'catid',$catid,'请选择',' class="form-control" ');
		return $this->tpl(get_defined_vars());
	}

	public function mod(){
		
		$get=$_GET;
		extract($get);
		$itemid=intval($itemid);
		if(!$itemid){
			$this->msg(0,'lose itemid');
			return;
		}

		$raw=$post=$_POST;
		$model=model('article');
		if($post){
			unset($post['submit'],$post['UNIQ'],$post['FORWARD']);
			$post=\dtrim($post);
			$post['edittime']=time();
			$post['editorid']=$this->auth['userid'];
			$post['editor']=$this->auth['username'];

			if(intval($post['catid'])==0){
				$this->msg(0,'所属分类不能为空');
				return;
			}
			
			if(!$post['title']){
				$this->msg(0,'标题不能为空');
				return;
			}
			
			if(!$post['content']){
				$this->msg(0,'内容不能为空');
				return;
			}
			
			$r=$model->update($post,['itemid'=>$itemid]);
			if($r){
				$model2=model('article_attachment');
				$model2->update(['articleid'=>$itemid],['uniq'=>$raw['UNIQ']]);
				$model->treat_attachment($itemid);
			}
			$this->msg($r,'',$raw['FORWARD']);
			return;
		}
		$data=$model->find('*',['itemid'=>$itemid]);
		if(!$data){
			$this->msg(0,'数据不存在');
			return;
		}
		$category_model=model('category');
		$category=$category_model->category_select($this->moduleid,'catid',$data['catid'],'请选择',' class="form-control" ');
		return $this->tpl(get_defined_vars());
	}

	public function del(){
		
		$get=$_GET;
		extract($get);

		$itemid=intval($itemid);
		if(!$itemid){
			$this->msg(0,'lose itemid');
			return;
		}
		$model=model('article');
		$d=$model->find('*',['itemid'=>$itemid]);
		if(!$d){
			$this->msg(0,'数据不存在');
			return;
		}
		//删除thumb
		//删除附件
		if(false){
			$model=model('article_attachment');
			$attachments=$model->select('*',['articleid'=>$itemid]);
			foreach($attachments as $k=>$v){
				$file=ROOTPATH.'public'.$v['url'];
				if(!file_exists($file)) continue;
				unlink($file);
			}
			$model->delete(['articleid'=>$itemid]);
		}
		$r=$model->update(['flag'=>3],['itemid'=>$itemid]);//$d->destroy();
		$this->msg($r===false?false:true);
		return;
	}

	public function preview(){
		
		$get=$_GET;
		extract($get);
		$itemid=intval($itemid);
		if(!$itemid){
			$this->msg(0,'lose itemid');
			return;
		}
		$model=model('article');
		$data=$model->find('*',['itemid'=>$itemid]);
		if(!$data){
			$this->msg(0,'数据不存在');
			return;
		}
		$category_model=model('category');
		$cat=$category_model->get_category($data['catid']);
		return $this->tpl(get_defined_vars());
	}

	public function outside(){
		$param=$_GET;
		extract($param);

		if(!$callback){
			$this->msg(0,'lose callback');
			return;
		}

		$model = model('article');
		$category_model=model('category');
		$where = [];
		$where['flag']=[[1,2],'in'];
		isset($itemid) or $itemid='';
		if($itemid){
			$where['itemid']=$itemid;
		}
		isset($catid) or $catid=0;
		if($catid){
			$where['catid']=$catid;
		}
		isset($title) or $title='';
		if($title){
			$where['title']=["%{$title}%",'like'];
		}
		isset($flag) or $flag=0;
		if($flag){
			$where['flag']=$flag;
		}
		$page = $param['page']??1;
		$pagesize = $this->setting['pagesize'];

		$where=$model->where($where,'1=1');
		$data = $model->db()->select('*')->from('szh_article')->where($where)->orderByDesc(['itemid'])->limit($pagesize)->offset(($page-1)*$pagesize)->query();
		foreach($data as $k=>$v){
			$data[$k]['cat']=$category_model->get_category($v['catid']);
			$data[$k]['addtime']=\timeswitch($v['addtime']);
		}
		$total = $model->value('count(*) as total',$where);
        $pager=pagination($_SERVER['REQUEST_URI'],$param,$page,$pagesize,$total);
		$category=$category_model->category_select($this->moduleid,'catid',$catid,'请选择',' class="form-control" ');
		return $this->tpl(get_defined_vars());
	}

	public function setting(){
		
		$post = $_POST;
		if($post){
			$r=\save_setting($this->moduleid,$post);
			$this->msg($r);
		}
		$setting=\load_setting($this->moduleid);
		return $this->tpl(get_defined_vars());
	}
	public function clear_attachments(){
		$model=model('article_attachment');
		$where=[];
		$where['articleid']=0;
		$where[]=[' ts < '.(time()-86400),'exp'];
		$attachments=$model->select('*',$where);
		foreach($attachments as $k=>$v){
			$file=ROOTPATH.'public'.$v['url'];
			if(!file_exists($file)) continue;
			unlink($file);
		}
		$model->delete($where);
		$this->msg(1);
		return;
	}

	public function test(){
        
        return $this->tpl(get_defined_vars());
    }
	
}