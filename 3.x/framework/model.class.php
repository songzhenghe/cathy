<?php
class model{
	protected $db;
	protected $cfg;
	protected $tb;
	public function __construct(){
		global $db;
		global $CFG;

		$this->db=$db;
		$this->cfg=$CFG['db'];
		$this->tb=str_replace('_model','',strtolower(get_class($this)));
	}
	public function db(){
		return $this->db;
	}
	//原生查询
	public function query($sql){
		return $this->db->query($sql);
	}
	//table
	public function table($tb){
		$this->tb=$tb;
		return $this;
	}
	//where
	public function where($arr,$before=''){
		if(!$arr) return $before;
		$sql=[];
		if($before) $sql[]=$before;
		
		foreach($arr as $k=>$v){
			if(is_array($v)){
				if(!isset($v[1])) $v[1]='=';

				if(strtolower($v[1])=='like'){
					$sql[]=" {$k} like '{$v[0]}' ";
					continue;
				}

				if(strtolower($v[1])=='in'){
					if(!is_array($v[0])) $v[0]=[$v[0]];
					$v[0][]=0;
					$v[0]=implode(',',$v[0]);
					$sql[]=" {$k} in({$v[0]}) ";
					continue;
				}

				if(strtolower($v[1])=='exp'){
					$sql[]=" {$v[0]} ";
					continue;
				}

				$sql[]=" {$k} {$v[1]} '{$v[0]}' ";
				continue;
			}
			$sql[]=" {$k}='{$v}' ";
		}

		return implode(' and ',$sql);
	}
	//order
	public function order($str){
		return " order by {$str} ";
	}
	//插入并返回id
	public function insert($data=[],$tb=''){
		//tablename
		if(!$tb) $tb=$this->tb;
		$table=$this->cfg['prefix'].$tb;
		$fds=$this->fields($table);
		//过滤post的数据
		$newdata=array_filter($data,function($v,$k) use ($fds){
			return in_array($k,$fds);
		},ARRAY_FILTER_USE_BOTH);

		$insertid=$this->db->insert($table)->cols($newdata)->query();
		return $insertid;
	}
	//更新并返回影响行数
	public function update($data,$where,$tb=''){
		if(!$where) return false;
		//tablename
		if(!$tb) $tb=$this->tb;
		$table=$this->cfg['prefix'].$tb;
		$fds=$this->fields($table);
		//过滤post的数据
		$newdata=array_filter($data,function($v,$k) use ($fds){
			return in_array($k,$fds);
		},ARRAY_FILTER_USE_BOTH);

		if(is_array($where)) $where=$this->where($where);
		$row_count=$this->db->update($table)->cols($newdata)->where($where)->query();
		return $row_count;
	}
	//删除
	public function delete($where,$tb=''){
		if(!$where) return false;
		//tablename
		if(!$tb) $tb=$this->tb;
		$table=$this->cfg['prefix'].$tb;

		if(is_array($where)) $where=$this->where($where);
		$row_count = $this->db->delete($table)->where($where)->query();
		return $row_count;
	}
	//查询多条
	public function select($fd='*',$where='1=1',$tb=''){
		//tablename
		if(!$tb) $tb=$this->tb;
		$table=$this->cfg['prefix'].$tb;

		if(is_array($where)) $where=$this->where($where);
		$data=$this->db->select($fd)->from($table)->where($where)->query();
		if(!is_array($data)) return array();
		return $data;
	}

	//查询一条
	public function find($fd='*',$where='1=1',$tb=''){
		//tablename
		if(!$tb) $tb=$this->tb;
		$table=$this->cfg['prefix'].$tb;

		if(is_array($where)) $where=$this->where($where);
		$data=$this->db->select($fd)->from($table)->where($where)->row();
		if(!is_array($data)) return array();
		return $data;
	}
	//查询一列
	public function column($fd,$where='1=1',$tb=''){
		//tablename
		if(!$tb) $tb=$this->tb;
		$table=$this->cfg['prefix'].$tb;

		if(is_array($where)) $where=$this->where($where);
		$data=$this->db->select($fd)->from($table)->where($where)->column();
		if(!is_array($data)) return array();
		return $data;
	}
	//查询一个值
	public function value($fd,$where='1=1',$tb=''){
		//tablename
		if(!$tb) $tb=$this->tb;
		$table=$this->cfg['prefix'].$tb;

		if(is_array($where)) $where=$this->where($where);
		return $this->db->select($fd)->from($table)->where($where)->single();
	}
	//获取表的字段
	public function fields($table){
		$file=RUNTIMEPATH.'table/'.$table.'.php';
		if(file_exists($file)) return include($file);

		$fds=$this->query("select column_name from information_schema.columns where table_schema ='{$this->cfg['database']}' and table_name = '{$table}' ");
		
		$data=[];
		foreach($fds as $k=>$v){
			$data[]=$v['column_name'];
		}
		file_put_contents($file,"<?php return ".var_export($data,true).";");
		return $data;
	}
	
}