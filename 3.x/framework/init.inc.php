<?php
defined('BASEPATH') or exit('access denied');
isset($_GET['r']) or $_GET['r']='index/welcome';


//处理路由
$route=explode('/',trim($_GET['r'],'/'));
$route=array_map('trim',$route);
$route=array_map('strtolower',$route);

echo "visit {$route[0]}/{$route[1]}\n";

if(!(isset($route[0]) && $route[0])){
	$connection->close(error_404('route:0'));
	return;
}

if(!(isset($route[1]) && $route[1])){
	$connection->close(error_404('route:1'));
	return;
}

//包含controller
require_once BASEPATH.'controller.class.php';

//调用app的控制器
$class=APPPATH.'controllers/'.$route[0].'.class.php';
if(!file_exists($class)){
	$connection->close(error_404('file error'));
	return;
}
require_once $class;

//排除一些魔术方法
$deny_list=array('_before','_after','__construct','__destruct','__call','__callStatic','__get','__set','__isset','__unset','__sleep','__wakeup','__toString','__invoke','__set_state','__clone');
if(in_array($route[1],$deny_list)){
	$connection->close(error_404());
	return;
}

//add prop
$prop['MODULE']=$route[0];
$prop['ACTION']=$route[1];
$prop['TPL']=APPPATH.'views/'.$route[0].'/'.$route[1].'.php';
$prop['CFG']=$CFG;
$prop['CONN']=$connection;
$prop['FORWARD']=isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
$prop['realIP']=$_SERVER['HTTP_X_REAL_IP'];
$prop['db']=$db;

$object=new $route[0]($prop);

//判断方法是否存在
if(!method_exists($object,$route[1])){
	$connection->close(error_404());
	return;
}

//利用反射类判断方法是否可以调用
$ref=new ReflectionMethod($object,$route[1]);
if(!$ref->isPublic()){
	$connection->close(error_404());
	return;
}


//调用类下的方法
$func=$route[1];

if(method_exists($object,'_before')){
	$r=$object->_before();
}
if($r===false){
	return;
}

$d=$object->$func($connection,$data);

if($d===false){
	return;
}
if(method_exists($object,'_after')){
	$object->_after();
}

if(!$d) $d='ok '.time();
$connection->close($d);