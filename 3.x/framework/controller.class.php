<?php
class controller{
	public $MODULE;
	public $ACTION;
	public $TPL;
	public $CFG;
	public $CONN;
	public $FORWARD;
	public $realIP;
	public $db;
	
	public $template;

	protected $SETTING;
    protected $CORE_CONF;
    protected $ISAJAX=false;
    protected $ISGET=true;
    protected $ISPOST=false;
    protected $REQUEST_URI='';
    protected $USERAGENT='';
    protected $MM='';
    protected $CC='';
    protected $AA='';
	
	public function __construct($prop){
		foreach($prop as $k=>$v){
			$this->$k=$v;
		}
		
	}
	public function _before(){
		$config = [
			'view_path'	=>	APPPATH.'views/',
			'cache_path'	=>	realpath(__DIR__.'/..').'/runtime/template/',
			'view_suffix'   =>	'html',
		];
		$this->template=new \think\Template($config);
		//var_dump($_SERVER);
		//https://www.php.net/manual/zh/reserved.variables.server.php
		//载入core_enums
        require_once APPPATH.'core/core_enums.class.php';
        //载入core_conf
        require_once APPPATH.'core/core_conf.class.php';
		$this->CORE_CONF=new core_conf();
		
		//全局模板变量
		$this->SETTING=include APPPATH.'setting/1.php';
		
		$this->getView()->assign(['SETTING'=>$this->SETTING]);
		//forward
		$FORWARD=isset($_POST['FORWARD'])?$_POST['FORWARD']:'';
		if($FORWARD){
			$this->FORWARD=urldecode($FORWARD);
		}else{
			$this->FORWARD=(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
		}
		$this->getView()->assign(['FORWARD'=>$this->FORWARD,'CORE_CONF'=>$this->CORE_CONF]);

		//ISAJAX
		if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER["HTTP_X_REQUESTED_WITH"]) == 'xmlhttprequest'){
			$this->ISAJAX=true;
		}

		$this->REQUEST_URI=$_SERVER['REQUEST_URI'];

		$this->USERAGENT=isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';

		$r=explode('/',trim($_GET['r'],'/'));
		$r=array_map('trim',$r);
		
		$this->MM=strtolower($r[0]);
		$this->CC=strtolower($r[0]);
		$this->AA=strtolower($r[1]);

		$this->getView()->assign(['REQUEST_URI'=>$_SERVER['REQUEST_URI'],'MM'=>$this->MM,'CC'=>$this->CC,'AA'=>$this->AA]);

		$this->ISGET=$_SERVER['REQUEST_METHOD']=='GET';
		$this->ISPOST=$_SERVER['REQUEST_METHOD']=='POST';

		
		return true;
	}
	public function _after(){
		return true;
	}
	public function tpl($var,$tpl_path=''){
		//https://gitee.com/liu21st/think-template/tree/1.0/
		$f=APPPATH.'views/'.($tpl_path?$tpl_path:($this->MODULE.'/'.$this->ACTION)).'.html';
		if(!file_exists($f)){
			return "template not exists:".$f;
		}
		$var=array_merge($var,$this->template->get());
		ob_start();
		$this->template->fetch($tpl_path?$tpl_path:$this->MODULE.'/'.$this->ACTION,$var);
		$content=ob_get_contents();
		ob_end_clean();
		return $content;
	}
	protected function p($v){
		$this->CONN->send("<pre>".var_export($v,true)."</pre>");
	}
	protected function msg($r,$msg='',$url=''){
		$this->CONN->send(msg($r,$msg,$url));
	}
	protected function jump($url){
		$this->CONN->send(jump($url));
	}
	protected function json($data){
		$this->CONN->send(json_encode($data));
	}
	protected function getView(){
		return $this->template;
	}
}