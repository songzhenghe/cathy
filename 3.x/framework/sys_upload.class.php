<?php
//文件上传组件
/**
$config=array(
	'field_name'=>'表单字段名称',
	'upload_path'=>'文件上传后的存放文件夹',
	'allow_type'=>'允许上传的文件类型',//数组
	'max_size'=>'文件最大上传尺寸',
	'unit'=>'文件尺寸单位',
	'israndname'=>'是否随机文件名',
	'iscreatedir'=>'是否自动创建文件夹',
	'iscreatebydate'=>'是否根据日期创建目录',
	'isoverride'=>'文件存在，是否进行覆盖',
);
*/
/**
<?php
header('content-type:text/html;charset=utf-8');
include('upload.instance.php');
if($_POST['submit']){

	$config=array(
		'field_name'=>'myfile',
		'upload_path'=>'E:/wwwroot/instance/uploads/',
		'allow_type'=>array('.jpg','.pdf'),//数组
		'max_size'=>'1',
		'unit'=>'M',
		'israndname'=>false,
		'iscreatedir'=>true,
		'iscreatebydate'=>true,
		'isoverride'=>true
	);

	$r=upload_instance($config);
	echo '<pre>';
	print_r($r);
	echo '</pre>';

}

?>
<form method='post' enctype='multi-part/form-data'>
	<input type='file' name='myfile'/><br />
	<input type='submit' name='submit' />
	<input type="hidden" name="MAX_FILE_SIZE" value="10000000000000000000">
</form>
*/
class sys_upload{
	public function upload_instance($config){
		//消息提示
		$message=array(
			0=>'文件上传成功！',
			-1=>'文件上传失败！',
			-2=>'文件大小超过上传要求！',
			-3=>'文件类型错误！',
			-4=>'文件尺寸单位错误！',
			-5=>'上传文件夹目录不存在！',
			-6=>'创建文件夹失败！',
			-7=>'上传的临时文件不存在！',
			-8=>'要上传的文件已经存在！'
			
		);
		//初始化返回值
		$return=array(
			'origin_name'=>'',
			'new_name'=>'',
			'file_type'=>'',
			'file_size'=>'',
			'file_abspath'=>'',
			'file_relpath'=>'',
			'error_number'=>'',
			'error_message'=>''
			
		);
		
		//$file=$_FILES[$config['field_name']];
		$file=current($_FILES);//取第一个元素 ajax时有问题
		// $file_mime=$file['file_type'];
		// $mime_array=get_mime($config['allow_type']);
		// //检测文件mime类型
		// if(!in_array($file_mime,$mime_array)){
			// $return['error_number']=-3;
			// $return['error_message']=$message[-3];
			// return $return;
		// }
		//检测后缀
		$pathinfo=pathinfo($file['file_name']);
		if(!in_array('.'.$pathinfo['extension'],$config['allow_type'])){
			$return['error_number']=-3;
			$return['error_message']=$message[-3];
			return $return;
		}

		$config['unit']=strtoupper($config['unit']);
		if(!in_array($config['unit'],array('B','K','M','G','T'))){
			$return['error_number']=-4;
			$return['error_message']=$message[-4];
			return $return;
		}
		
		//检测文件尺寸2
		$file_size=$file['file_size'];
		switch($config['unit']){
			case 'B':
			
			break;
			case 'K':
			$config['max_size']*=1024;
			break;
			case 'M':
			$config['max_size']*=1048576;
			break;
			case 'G':
			$config['max_size']*=1073741824;
			break;
			case 'T':
			$config['max_size']*=1099511627776;
			break;
			
		}
		if($file_size>$config['max_size']){
			$return['error_number']=-2;
			$return['error_message']=$message[-2];
			return $return;
		}
		//生成文件夹名
		if($config['iscreatebydate']){
			$p=date('Y/m/d').'/';
			$config['upload_path']=$config['upload_path'].$p;
			$return['file_relpath']=$p;
		}
		
		//检测上传路径
		$config['upload_path']=rtrim($config['upload_path'],'/').'/';
		if(!$config['iscreatedir']){
			if(!file_exists($config['upload_path'])){
				$return['error_number']=-5;
				$return['error_message']=$message[-5];
				return $return;
			}		
		}
		
		//创建上传文件夹目录
		if($config['iscreatedir']){
			if(!is_dir($config['upload_path'])) mkdir($config['upload_path'],0777,true);
			if(!is_dir($config['upload_path'])){
				$return['error_number']=-6;
				$return['error_message']=$message[-6];
				return $return;
			}
		}
		$file_name=basename($file['file_name']);//文件名
		//生成新文件名
		$file_type=strrchr($file_name,'.');//文件后缀 .txt
		if($config['israndname']){
			$return['new_name']=time().rand(1000,9999).$file_type;
		}else{
			$return['new_name']=$file_name;
		}
		//文件是否存在
		if(!$config['isoverride']){
			if(file_exists($config['upload_path'].$return['new_name'])){
				$return['error_number']=-8;
				$return['error_message']=$message[-8];
				return $return;
			}
		}
		//正式上传
		if(!file_put_contents($config['upload_path'].$return['new_name'],$file['file_data'])){
			$return['error_number']=-1;
			$return['error_message']=$message[-1];
			return $return;
		}
		
		//整合返回值
		$return['origin_name']=$file_name;
		$return['file_type']=$file_type;
		$return['file_size']=$file_size;
		$return['file_abspath']=$config['upload_path'];
		$return['error_number']=0;
		$return['error_message']=$message[0];
		return $return;
	}

}

