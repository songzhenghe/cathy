<?php /*a:1:{s:47:"/home/szh/jessica/4.x/app/views/article/mod.php";i:1639706145;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	<title>网站内容管理系统</title>
	<?php include view('common/cssjs');?>
</head>
<body>
<?php include view('common/header');include view('common/left');?>

<div style="float:left;width:900px;">
<form method="post" id="myform">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td align="right">标题*</td>
    <td><input type="text" name="title" id="" value="<?php echo $title;?>"></td>
  </tr>
  <tr>
    <td align="right">内容*</td>
    <td><textarea name="content" id="" cols="30" rows="10"><?php echo $content;?></textarea></td>
  </tr>
  <tr>
    <td align="right">状态</td>
    <td>
      <?php
      foreach(core_enums::$article_flag as $k=>$v){
      ?>
      <label><input <?php if($k==$flag){ ?>checked<?php }?> type="radio" name="flag" value="<?php echo $k;?>" id=""> <?php echo $v;?></label>
      <?php
      }
      ?>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" value="提交" name="submit"></td>
    </tr>
</table>
</form>
</div>



<?php include view('common/footer');?>

<script>
$(document).ready(function(e) {
	$('#myform').submit(function(){
        
		var options = {
			data:{},
			type: "post",
			beforeSubmit: function(){},
			success: function(data){
				if(parseInt(data.code)>0){
					alert(data.msg);	
					location.href='<?php echo $request->header('referer');?>';
				}else{
					alert(data.msg);
				}
			},
			dataType: "json",
			clearForm: false,
			resetForm: false,
			timeout: 5000
		};
		$(this).ajaxSubmit(options);
		
		return false;	
	});
	
});
</script>
</body>
</html>