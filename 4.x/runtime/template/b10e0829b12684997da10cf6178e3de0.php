<?php /*a:2:{s:55:"/home/szh/cathy/4.x/app/views/upload/img_tradition.html";i:1640395825;s:47:"/home/szh/cathy/4.x/app/views/common/cssjs.html";i:1640395825;}*/ ?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php echo htmlentities($SETTING['website_name']); ?></title>

    <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/static/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/static/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="/static/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="/static/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/static/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/static/adminlte/dist/js/adminlte.min.js"></script>

  
<script type="text/javascript" src="/static/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<!-- $( '#editor1' ).ckeditor();$( '#editor1' ).val( $( 'input#val' ).val() ); -->

<script src="/static/WdatePicker/WdatePicker.js"></script>
<script src="/static/js/jquery.cookie.js" ></script>
<script src="/static/colorbox/jquery.colorbox-min.js"></script>
<script src="/static/js/jquery.form.js"></script>
<script src="/static/js/jquery.serializeExtend-1.0.1.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/vue-resource.min.js"></script>
<script src="/static/js/jquery.qrcode.min.js"></script>


</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
      <h3 style="text-align: center;">文件类型：jpg,png,gif最大2M</h3>
          
    <div style="padding:20px;text-align: center;">
      <input type="file" name="" id="file">
      <button id="upload">上传</button>
      <span id="output" style="font-size:12px">等待</span>
    </div>


  </div>

  

<script>
  //https://www.jb51.net/article/84188.htm
  $('#upload').click(function(){
    if(!$("#file")[0].files[0]){
      alert('请先选择文件');
      return;	
    }
    var file = $("#file")[0].files[0];
    

    var form = new FormData();
    form.append("file", file);
    form.append("class", '<?php echo htmlentities($class); ?>');
    form.append("i", '<?php echo htmlentities($i); ?>');
    form.append("callback", '<?php echo htmlentities($callback); ?>');
    form.append('uniq', '<?php echo htmlentities($uniq); ?>');
    //Ajax提交
    $.ajax({
      url: "<?php echo htmlentities($UPLOAD_URL3); ?>",
      type: "POST",
      data: form,
      async: true,        //异步
      processData: false,  //很重要，告诉jquery不要对form进行处理
      contentType: false,  //很重要，指定为false才能形成正确的Content-Type
      dataType: 'json',
      success: function(data){
        if(parseInt(data.result.code)!=0){
          alert(data.result.msg);
        }else{
            alert('上传成功');
            if(typeof(window.parent.<?php echo htmlentities($callback); ?>)!='undefined'){
              window.parent.<?php echo htmlentities($callback); ?>(data.result);
            }else{
              console.log(data);
            }          
        }
      },
      error: function(){
        alert('上传失败');	
      }
    });
    
  });
</script>  
</body>

</html>