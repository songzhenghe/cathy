<?php /*a:2:{s:49:"/home/szh/cathy/4.x/app/views/member/outside.html";i:1640411818;s:47:"/home/szh/cathy/4.x/app/views/common/cssjs.html";i:1640395825;}*/ ?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo htmlentities($SETTING['website_name']); ?></title>
    <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/static/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/static/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="/static/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="/static/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/static/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/static/adminlte/dist/js/adminlte.min.js"></script>

  
<script type="text/javascript" src="/static/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<!-- $( '#editor1' ).ckeditor();$( '#editor1' ).val( $( 'input#val' ).val() ); -->

<script src="/static/WdatePicker/WdatePicker.js"></script>
<script src="/static/js/jquery.cookie.js" ></script>
<script src="/static/colorbox/jquery.colorbox-min.js"></script>
<script src="/static/js/jquery.form.js"></script>
<script src="/static/js/jquery.serializeExtend-1.0.1.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/vue-resource.min.js"></script>
<script src="/static/js/jquery.qrcode.min.js"></script>


</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper"> 

            <!-- Default box -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">会员列表</h3>
                </div>
                <div class="card-body p-0">
                  <form action="" method="get">
                    <input type="hidden" name="r" value="<?php echo htmlentities($R); ?>">
                    <input type="hidden" name="callback" value="<?php echo htmlentities($callback); ?>">
                    <table class="table table-bordered">
                      <tr>
                        <td>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">ID</span>
                            </div>
                            <input type="text" name="userid" class="" placeholder="" value="<?php echo htmlentities($userid); ?>">
                            &nbsp;
                            <div class="input-group-prepend">
                              <span class="input-group-text">账号</span>
                            </div>
                            <input type="text" name="username" class="" placeholder="" value="<?php echo htmlentities($username); ?>">
                            &nbsp;
                            <div class="input-group-prepend">
                              <span class="input-group-text">所属组</span>
                            </div>
                            <select class="" name="groupid">
                                <option value="0">请选择</option>
                                <?php foreach(\core_enums::$member_group as $k=>$v): ?>
                                <option value="<?php echo htmlentities($k); ?>" <?php if($groupid==$k): ?>selected<?php endif; ?>><?php echo htmlentities($v); ?></option>
                                <?php endforeach; ?>
                            </select>
                            &nbsp;
                            <div class="input-group-prepend">
                              <span class="input-group-text">真实姓名</span>
                            </div>
                            <input type="text" name="truename" class="" placeholder="" value="<?php echo htmlentities($truename); ?>">
                            &nbsp;
                            <button name="submit" type="submit" class="btn btn-primary">搜索</button>
                            &nbsp;
                            <button type="button" class="btn btn-warning" onclick="location.href='?r=member/outside&callback=<?php echo htmlentities($callback); ?>';">重置</button>
                          </div>
      
                        </td>
                      </tr>
                    </table>
                  </form>
                  
                  <table class="table table-bordered">
                    <?php if(count($data)): ?>
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>账号</th>
                        <th>所属组</th>
                        <th>真实姓名</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data as $k=>$v): 
                      unset($v['password']);
                      ?>
                      <tr>
                        <td><?php echo htmlentities($v['userid']); ?></td>
                        <td><a href='javascript:my_click(<?php echo json_encode($v); ?>);'><?php echo htmlentities($v['username']); ?></a></td>
                        <td><?php echo myswitch($v['groupid'],core_enums::$member_group); ?></td>
                        <td><?php echo htmlentities($v['truename']); ?></td>
                      </tr>
                      <?php endforeach; ?>
                      <tr>
                        <td colspan="100">
                          <?php echo $pager; ?>
                        </td>
                      </tr>
                      <?php else: ?>
                      <tr>
                        <td colspan="100">暂无数据</td>
                      </tr>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->



  </div>
<script>
function my_click(arr){
  parent.<?php echo htmlentities($callback); ?>(arr);	
}
</script>
</body>

</html>