<?php /*a:1:{s:47:"/home/szh/jessica/4.x/app/views/article/lst.php";i:1639702556;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	<title>网站内容管理系统</title>
	<?php include view('common/cssjs');?>
</head>
<body>
<?php include view('common/header');include view('common/left');?>

<div style="float:left;">
<form>
<input type="hidden" name="r" value="<?php echo $R;?>">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td>
      标题：
      <input type="text" name="title" id="" value="<?php echo $title;?>"> 
      状态：
      <select name="flag" id="">
      <option value="0">请选择</option>
      <?php
      foreach(core_enums::$article_flag as $k=>$v){
      ?>
      <option <?php if($k==$flag){?>selected<?php }?> value="<?php echo $k;?>"><?php echo $v;?></option>
      <?php
      }
      ?>
      </select>
      </td>
  </tr>
  <tr>
    <td align="center"><input type="submit" name="submit" value="搜索"> <input type="button" value="重置" onClick="location.href='?r=article/lst';" ></td>
  </tr>
</table>
</form>
<table width="100%" border="0" class="table table-bordered">
<?php if(count($lst)){?>
  <tr>
    <td>ID</td>
    <td>标题</td>
    <td>添加时间</td>
    <td>编辑时间</td>
    <td>状态</td>
    <td>编辑</td>
    <td>删除</td>
  </tr>
  <?php foreach($lst as $k=>$v){?>
  <tr>
    <td><?php echo $v['itemid'];?></td>
    <td><?php echo $v['title'];?></td>
    <td><?php echo $v['addtime'];?></td>
    <td><?php echo $v['edittime'];?></td>
    <td><?php echo $v['flag'];?></td>
    <td><a href="?r=article/mod&itemid=<?php echo $v['itemid'];?>" class="">编辑</a></td>
    <td><a onclick="return confirm('确定要删除吗?');" href="?r=article/del&itemid=<?php echo $v['itemid'];?>" class="">删除</a></td>
  </tr>
  <?php
  }
  ?>
  <tr>
    <td colspan="6"><?php echo $pager;?></td>
  </tr>
  <?php }else{?>
  <tr>
    <td colspan="6">暂无数据</td>
  </tr>
  <?php }?>
</table>

</div>

<?php include view('common/footer');?>
</body>
</html>