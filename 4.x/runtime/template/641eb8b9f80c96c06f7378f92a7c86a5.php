<?php /*a:5:{s:45:"/home/szh/cathy/4.x/app/views/member/lst.html";i:1640409765;s:47:"/home/szh/cathy/4.x/app/views/common/cssjs.html";i:1640395825;s:48:"/home/szh/cathy/4.x/app/views/common/navbar.html";i:1640395825;s:49:"/home/szh/cathy/4.x/app/views/common/sidebar.html";i:1640395825;s:48:"/home/szh/cathy/4.x/app/views/common/footer.html";i:1640395825;}*/ ?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo htmlentities($SETTING['website_name']); ?></title>
    <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/static/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/static/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="/static/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="/static/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/static/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/static/adminlte/dist/js/adminlte.min.js"></script>

  
<script type="text/javascript" src="/static/ckeditor/ckeditor.js?t=C3HA5RM"></script>
<script type="text/javascript" src="/static/ckeditor/adapters/jquery.js"></script>
<!-- $( '#editor1' ).ckeditor();$( '#editor1' ).val( $( 'input#val' ).val() ); -->

<script src="/static/WdatePicker/WdatePicker.js"></script>
<script src="/static/js/jquery.cookie.js" ></script>
<script src="/static/colorbox/jquery.colorbox-min.js"></script>
<script src="/static/js/jquery.form.js"></script>
<script src="/static/js/jquery.serializeExtend-1.0.1.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/vue-resource.min.js"></script>
<script src="/static/js/jquery.qrcode.min.js"></script>


</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">   <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/static/adminlte/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/static/adminlte/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="/static/adminlte/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fas fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="javascript:void(0);" class="brand-link">
    <img src="/static/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8">
    <span class="brand-text font-weight-light"><?php echo htmlentities($SETTING['website_name']); ?></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="/static/adminlte/dist/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block"><?php echo htmlentities($ADMIN['username']); ?>，你好！</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              欢迎使用
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="?r=frameset/index" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>后台首页</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="?r=frameset/logout" class="nav-link" onclick="return confirm('确定要退出么？');">
                <i class="far fa-circle nav-icon"></i>
                <p>安全退出</p>
              </a>
            </li>
          </ul>
        </li>
        <?php
        $_MENU=$CORE_CONF->user_menu($ADMIN['groupid'],$ADMIN['userid'],$CORE_CONF->MENU);
        foreach($CORE_CONF->MODULE as $_moduleid=>$_module): 
        if(empty($_MENU[$_moduleid])) continue;
        $_menu=$_MENU[$_moduleid];
        ?>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                <?php echo htmlentities($_module['module_cn']); ?>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            
            <ul class="nav nav-treeview">
              <?php foreach($_menu as $_m): 
              if(isset($_m['url'])){
                $_url=$_m['url'];
              }else{
                $_url="?r=".$_module['module_en']."/".$_m['name'];
              }
              ?>
              <li class="nav-item">
                <a href="<?php echo htmlentities($_url); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p><?php echo htmlentities($_m['chinese']); ?></p>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
          </li>
          <?php endforeach; ?>  
          <!--
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              系统设置
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="?r=admin/safe" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>修改密码</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=admin/setting" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>网站设置</p>
              </a>
            </li>

          </ul>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              会员管理
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="?r=member/add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>添加会员</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=member/lst" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>会员列表</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=member/login_log" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>登录日志</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=member/setting" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>模块设置</p>
              </a>
            </li>

          </ul>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              文章管理
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="?r=article/add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>添加文章</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=article/lst" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>文章列表</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=article/category_add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>添加分类</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=article/category_lst" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>分类列表</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="?r=article/setting" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>模块设置</p>
              </a>
            </li>

            <li class="nav-item">
                <a href="?r=article/clear_attachments" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>清理垃圾附件</p>
                </a>
              </li>

            <li class="nav-item">
                <a href="?r=article/test" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>测试</p>
                </a>
              </li>

          </ul>
        </li>
      -->
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>

<script>
    $(document).ready(function(e) {
	  $.ajaxSetup({async:false});
      $.post('?r=frameset/get_menu_current',{},function(data){
        $('ul.nav-sidebar > li.has-treeview').eq(parseInt(data.result.a)).children('a:first').trigger('click');
        if(data.result.href!='#'){
          $('a.nav-link').each(function(index,element){
            if($(element).attr('href')==data.result.href) $(element).addClass('active');
          });
        }
      },'json');
    
      $('ul.nav-sidebar > li.has-treeview > a[href="#"]').click(function(){
        var a=parseInt($(this).parent().index('.has-treeview'));
        $.post('?r=frameset/set_menu_current',{a:a,href:'#'},function(data){}
  
        ,'json');
      });
  
      $('ul.nav-sidebar > li.has-treeview > ul.nav-treeview > li.nav-item > a.nav-link').click(function(){
        var a=parseInt($(this).parent().parent().parent().index('.has-treeview'));
        $.post('?r=frameset/set_menu_current',{a:a,href:$(this).attr('href')},function(data){}
  
        ,'json');
      });
	  $.ajaxSetup({async:true});
    });
  </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">会员列表</h3>
          </div>
          <div class="card-body p-0">
            <form action="" method="get">
              <input type="hidden" name="r" value="<?php echo htmlentities($R); ?>">
              <table class="table table-bordered">
                <tr>
                  <td>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">ID</span>
                      </div>
                      <input type="text" name="userid" class="" placeholder="" value="<?php echo htmlentities($userid); ?>">
                      &nbsp;
                      <div class="input-group-prepend">
                        <span class="input-group-text">账号</span>
                      </div>
                      <input type="text" name="username" class="" placeholder="" value="<?php echo htmlentities($username); ?>">
                      &nbsp;
                      <div class="input-group-prepend">
                        <span class="input-group-text">所属组</span>
                      </div>
                      <select class="" name="groupid">
                          <option value="0">请选择</option>
                          <?php foreach(\core_enums::$member_group as $k=>$v): ?>
                          <option value="<?php echo htmlentities($k); ?>" <?php if($groupid==$k): ?>selected<?php endif; ?>><?php echo htmlentities($v); ?></option>
                          <?php endforeach; ?>
                      </select>
                      &nbsp;
                      <div class="input-group-prepend">
                        <span class="input-group-text">真实姓名</span>
                      </div>
                      <input type="text" name="truename" class="" placeholder="" value="<?php echo htmlentities($truename); ?>">
                      &nbsp;
                      <button name="submit" type="submit" class="btn btn-primary">搜索</button>
                      &nbsp;
                      <button type="button" class="btn btn-warning" onclick="location.href='?r=member/lst';">重置</button>
                    </div>

                  </td>
                </tr>
              </table>
            </form>
            
            <table class="table table-bordered">
              <?php if(count($data)): ?>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>账号</th>
                  <th>所属组</th>
                  <th>真实姓名</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($data as $k=>$v): ?>
                <tr>
                  <td><?php echo htmlentities($v['userid']); ?></td>
                  <td><?php echo htmlentities($v['username']); ?></td>
                  <td><?php echo myswitch($v['groupid'],core_enums::$member_group); ?></td>
                  <td><?php echo htmlentities($v['truename']); ?></td>
                  <td>
                      <a class="btn btn-warning btn-sm" href="?r=member/authenticate&userid=<?php echo htmlentities($v['userid']); ?>"> <i class="fas fa-lock">
                        </i> 权限 </a>
                    <a class="btn btn-info btn-sm" href="?r=member/mod&userid=<?php echo htmlentities($v['userid']); ?>"> <i class="fas fa-pencil-alt">
                      </i> 编辑 </a>
                    <a onclick="return confirm('确定要禁用么？');" class="btn btn-danger btn-sm" href="?r=member/del&userid=<?php echo htmlentities($v['userid']); ?>"> <i class="fas fa-trash">
                      </i> 禁用 </a>
                  </td>
                </tr>
                <?php endforeach; ?>
                <tr>
                  <td colspan="100">
                    <?php echo $pager; ?>
                  </td>
                </tr>
                <?php else: ?>
                <tr>
                  <td colspan="100">暂无数据</td>
                </tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">szh</a>.</strong> All rights reserved.
  </footer>

  <script>
  //表单重置
  $(document).ready(function(){
    $('form').each(function(index,element){
      $(element).get(0).reset();
    });
  });
  </script>
  </div>
</body>

</html>