<?php
$config=array();
//基本配置
$config['base']=array(
    //网站域名
    'base_url'=>'http://cathy.com/',
    'listen'=>'http://0.0.0.0:2345',
    'name'=>'cathy',
    'count'=>4,
);

$config['db']=[
    'default'    =>    'mysql',
    'connections'    =>    [
        'mysql'    =>    [
            // 数据库类型
            'type'        => 'mysql',
            // 服务器地址
            'hostname'    => '127.0.0.1',
            // 数据库名
            'database'    => 'cathy',
            // 数据库用户名
            'username'    => 'root',
            // 数据库密码
            'password'    => 'root',
            // 数据库连接端口
            'hostport'    => '3306',
            // 数据库连接参数
            'params'      => [],
            // 数据库编码默认采用utf8
            'charset'     => 'utf8',
            // 数据库表前缀
            'prefix'      => 'szh_',
            'fields_strict'=>false,
            'debug'=>true,
        ],
    ],
];

$config['cache']=[
    'scheme' => 'tcp',
    'host' => '127.0.0.1',
    'port' => 6379,
    'password'=>'77h&Ukqbf^^f&MRc',
    'database'=>0,
];
return $config;
