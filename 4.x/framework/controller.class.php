<?php
class controller
{
    public $R;
    public $MODULE;
    public $ACTION;
    public $TPL;
    public $CFG;
    public $CONN;
    public $REQUEST;
    public $RESPONSE;
    
    public $template;

    protected $SETTING;
    protected $CORE_CONF;
    
    public function __construct($prop)
    {
        foreach ($prop as $k=>$v) {
            $this->$k=$v;
        }
    }
    public function _before($request,$response)
    {
        $config = [
            'view_path'	=>	APPPATH.'views/',
            'cache_path'	=>	realpath(__DIR__.'/..').'/runtime/template/',
            'view_suffix'   =>	'html',
        ];
        $this->template=new \think\Template($config);

        //载入core_enums
        require_once APPPATH.'core/core_enums.class.php';
        //载入core_conf
        require_once APPPATH.'core/core_conf.class.php';
        $this->CORE_CONF=new core_conf();
        
        //全局模板变量
        $this->SETTING=include APPPATH.'setting/1.php';
        
        $this->getView()->assign(['R'=>$this->R,'MODULE'=>$this->MODULE,'ACTION'=>$this->ACTION,'SETTING'=>$this->SETTING,'CORE_CONF'=>$this->CORE_CONF]);

        return true;
    }
    public function _after($request,$response)
    {
        return true;
    }
    public function tpl($var, $tpl_path='')
    {
        //https://gitee.com/liu21st/think-template/tree/1.0/
        $f=APPPATH.'views/'.($tpl_path?$tpl_path:($this->MODULE.'/'.$this->ACTION)).'.html';
        if (!file_exists($f)) {
            return "template not exists:".$f;
        }
        $var=array_merge($var, $this->template->get());
        ob_start();
        $this->template->fetch($tpl_path?$tpl_path:$this->MODULE.'/'.$this->ACTION, $var);
        $content=ob_get_contents();
        ob_end_clean();
        return $content;
    }
    protected function p($v)
    {
        return "<pre>".var_export($v, true)."</pre>";
    }
    protected function msg($r, $msg='', $url='', $data=[])
    {
        $str=$this->REQUEST->header('x-requested-with');
        return msg($r, $msg, $url, $data, $str && strtolower($str)=='xmlhttprequest');
    }
    protected function jump($url)
    {
        return jump($url);
    }
    protected function json($data)
    {
        return json_encode($data);
    }
    protected function getView()
    {
        return $this->template;
    }
}
