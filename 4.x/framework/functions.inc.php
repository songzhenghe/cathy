<?php
//返回模板内容
function load_template($_name, $_var=[])
{
    if (!file_exists($_name)) {
        return "{$_name} not exist";
    }
    extract($_var);
    ob_start();
    include $_name;
    $_content=ob_get_contents();
    ob_end_clean();
    return $_content;
}
//调试函数
function p($var)
{
    var_dump($var);
}

//自动加载类文件
function my_autoloader($class)
{
    $a=BASEPATH.$class.'.class.php';
    $b=APPPATH.'core/'.$class.'.class.php';
    $c=APPPATH.'models/'.$class.'.class.php';
    if (file_exists($a)) {
        include_once $a;
    }
    if (file_exists($b)) {
        include_once $b;
    }
    if (file_exists($c)) {
        include_once $c;
    }
}
//路由没有找到时的提示
function error_404($message='')
{
    return new Workerman\Protocols\Http\Response(404, [], '404 没有找到操作'.$message);
}
//加载视图
function view($name)
{
    return APPPATH.'views/'.$name.'.php';
}
//加载模型
function model($name)
{
    $class=$name.'_model';
    require_once APPPATH.'models/'.$class.'.class.php';
    return new $class();
}
function msg($r, $msg='', $url='', $data=[], $isajax=false)
{
    if ($r and !$msg) {
        $msg='操作成功';
    }
    if (!$r and !$msg) {
        $msg='操作失败';
    }

    if (!$isajax) {
        $str='';
        $str.="<h2 style='width:400px;height:30px;line-height:30px;text-align:center;background:skinblue;border:1px solid green;margin:0 auto;'>{$msg}</h2>";
        if (!$r) {
            $str.="<script>setTimeout(function(){history.back();},3000);</script>";
        }
        if ($r and $url) {
            if (strpos($url, 'http')!==false) {
            } else {
                $url='?r='.$url;
            }
            $str.="<script>setTimeout(function(){location.href='{$url}';},1000);</script>";
        }
        if ($r and !$url) {
            $str.="<script>setTimeout(function(){history.back();},3000);</script>";
        }
    } else {
        $d=[
            'code'=>(int) $r,
            'msg'=>$msg,
            'url'=>$url,
            'data'=>$data,
        ];
        $str=json_encode($d);
    }
    return $str;
}
function jump($url)
{
    if (strpos($url, 'http')!==false) {
    } else {
        $url='?r='.$url;
    }
    $str="<script>location.href='{$url}';</script>";
    return $str;
}
function mydtrim($arr)
{
    if (is_array($arr)) {
        foreach ($arr as $k=>$v) {
            $arr[$k]=mydtrim($v);
        }
    } else {
        $arr=trim($arr);
    }
    return $arr;
}
function myswitch($kvalue, $array)
{
    return  isset($array[$kvalue])?$array[$kvalue]:'';
}
function timeswitch($ts, $format='Y-m-d', $default='')
{
    return $ts?date($format, $ts):$default;
}
function uniq()
{
    mt_srand();
    return md5(time().rand(100000, 999999));
}

function copy_setting($moduleid)
{
    $setting_file=APPPATH."setting/{$moduleid}.php";
    if (!file_exists($setting_file)) {
        copy(APPPATH.'setting/0.php', APPPATH."setting/{$moduleid}.php");
    }
}
function load_setting($moduleid)
{
    $setting_file=APPPATH."setting/{$moduleid}.php";
    $setting=array();
    if (file_exists($setting_file)) {
        $setting=include($setting_file);
    }
    return $setting;
}
function save_setting($moduleid, $post)
{
    $setting_file=APPPATH."setting/{$moduleid}.php";
    if ($post) {
        return array_save($post, $setting_file);
    }
    return false;
}
function array_save($array, $file)
{
    $data = var_export($array, true);
    $data = "<?php\n return ".$data.';';
    return file_put_contents($file, $data);
}
function pagination($url, $param, $page, $pagesize, $total)
{
    $total_page=ceil($total/$pagesize);
    $previous_disabled=$next_disabled="";
    if ($page<=1) {
        $previous_disabled="disabled";
    }
    if ($page>=$total_page) {
        $next_disabled="disabled";
    }
    $previous_page=max($page-1, 1);
    $next_page=min($page+1, $total);

    $p1=http_build_query(array_merge($param, ['page'=>$previous_page]));
    $p2=http_build_query(array_merge($param, ['page'=>$next_page]));

    if (strpos($url, '?')!==false) {
        $url1=$url.'&'.$p1;
        $url2=$url.'&'.$p2;
    } else {
        $url1=$url.'?'.$p1;
        $url2=$url.'?'.$p2;
    }

    $html=<<<st
	<div class="paging_simple_numbers" style="width:400px;margin:0 auto;">
	<ul class="pagination">
	  <li class="paginate_button page-item previous {$previous_disabled}">
	  <a href="{$url1}" class="page-link">上一页</a>
	  </li>
	  <li class="paginate_button page-item active">
	  <a href="javascript:void(0);" class="page-link">{$page}/{$total_page} [共{$total}条]</a>
	  </li>
	  <li class="paginate_button page-item next {$next_disabled}">
	  <a href="{$url2}" class="page-link">下一页</a>
	  </li>
	</ul>
  </div>
st;
    return $html;
}
function get_in_str($arr, $def=null)
{
    isset($arr) or $arr=array();
    $arr[]=0;
    if ($def!==null) {
        $arr[]=$def;
    }
    $arr=array_map('intval', $arr);
    $str=implode(',', $arr);
    return $str;
}
function array_key_change($arr, $fun='strtolower')
{
    $ret=array();
    foreach ($arr as $key => $value) {
        $ret[$fun($key)]=$value;
    }
    return $ret;
}
//二维数组转一维
function multi2single($array)
{
    $new_array=array();
    $keys=array_keys($array[0]);
    $size=count($keys);
    $n=count($array);
    for ($i=0;$i<$n;$i++) {
        for ($j=0;$j<$size;$j++) {
            $key_name=$keys[$j];
            $new_array[$key_name][]=$array[$i][$key_name];
        }
    }
    return $new_array;
}
//二维转一维 单元素
function multi2single_min($array, $key_name)
{
    $new_array=array();
    if (!is_array($array)) {
        return array();
    }
    if (!is_array($array[0])) {
        return array();
    }
    $n=count($array);
    for ($i=0;$i<$n;$i++) {
        $new_array[]=$array[$i][$key_name];
    }
    return $new_array;
}

function dhtmlspecialchars($string)
{
    if (is_array($string)) {
        return array_map('dhtmlspecialchars', $string);
    } else {
        $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
        return str_replace('&amp;', '&', $string);
    }
}
function dtrim($arr)
{
    if (is_array($arr)) {
        foreach ($arr as $k=>$v) {
            $arr[$k]=dtrim($v);
        }
    } else {
        $arr=trim($arr);
    }
    return $arr;
}
function kv_builder($arr, $id, $name, $add=array(), $prefix=array())
{
    $res=array();
    if (count($prefix)) {
        foreach ((array) $prefix as $v) {
            $res[$v[0]]=$v[1];
        }
    }
    foreach ((array) $arr as $v) {
        $res[$v[$id]]=$v[$name];
    }
    if (count($add)) {
        foreach ((array) $add as $v) {
            $res[$v[0]]=$v[1];
        }
    }
    return $res;
}
function ka_builder($arr, $id, $name='*', $add=array())
{
    $res=array();
    $name_arr=explode(',', $name);
    foreach ((array) $arr as $v) {
        if ($name_arr[0]=='*') {
            $res[$v[$id]]=$v;
        } else {
            $tmp=array();
            foreach ($v as $kk=>$vv) {
                if (in_array($kk, $name_arr)) {
                    $tmp[$kk]=$vv;
                }
            }
            $res[$v[$id]]=$tmp;
        }
    }
    if (count($add)) {
        foreach ((array) $add as $v) {
            $res[$v[0]]=$v;
        }
    }
    return $res;
}
function kc_builder($arr, $name, $add=array())
{
    $res=array();
    foreach ((array) $arr as $v) {
        $res[]=$v[$name];
    }
    if (count($add)) {
        foreach ((array) $add as $v) {
            $res[]=$v;
        }
    }
    return $res;
}
function ks_builder($arr, $id, $name='*', $sep='-', $add=array())
{
    $res=array();
    $name_arr=explode(',', $name);
    foreach ((array) $arr as $v) {
        if ($name_arr[0]=='*') {
            $res[$v[$id]]=implode($sep, $v);
        } else {
            $tmp=array();
            foreach ($v as $kk=>$vv) {
                if (in_array($kk, $name_arr)) {
                    $tmp[$kk]=$vv;
                }
            }
            $res[$v[$id]]=implode($sep, $tmp);
        }
    }
    if (count($add)) {
        foreach ((array) $add as $v) {
            $res[$v[0]]=$v[1];
        }
    }
    return $res;
}

function post($url, $post_data)
{
    //初始化
    $curl = curl_init();
    //设置抓取的url
    curl_setopt($curl, CURLOPT_URL, $url);
    //设置头文件的信息作为数据流输出
    curl_setopt($curl, CURLOPT_HEADER, 0);
    //设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //设置post方式提交
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));
    //执行命令
    $data = curl_exec($curl);
    //关闭URL请求
    curl_close($curl);
    //显示获得的数据
    return $data;
}
