<?php
class article_model extends common_model
{
    protected $name='article';
    protected $pk = 'itemid';
    //处理编辑时的垃圾附件
    public function treat_attachment($pk)
    {
        $data=$this->where(['itemid'=>$pk])->find();
        $string=$data['thumb'].$data['content'];

        $model=model('article_attachment');
        $attachments=$model->where(['articleid'=>$pk])->select();
        foreach ($attachments as $k=>$v) {
            if (strpos($string, $v['url'])!==false) {
                continue;
            }

            $file=ROOTPATH.'public'.$v['url'];
            if (!$v['url'] || !file_exists($file)) {
                continue;
            }
            @unlink($file);
            \think\facade\Db::name('article_attachment')->where(['itemid'=>$v['itemid']])->delete();
        }
        return true;
    }
}
