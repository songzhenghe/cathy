<?php
class access_back_model extends common_model
{

    protected $name='access_back';
    protected $pk = 'itemid';
    //后台权限
    public function set_auth($userid,$post){
        isset($post['access']) or $post['access']=[];
        $this->where(['userid'=>$userid,'moduleid'=>$post['moduleid']])->delete();

        foreach($post['access'] as $controller=>$actions){
            foreach($actions as $action){
                $this->insert(['userid'=>$userid,'moduleid'=>$post['moduleid'],'module'=>$post['module'],'controller'=>$controller,'action'=>$action]);
            }
        }
        return true;
    }
    public function get_auth($userid,$moduleid){
        $data=$this->where(['userid'=>$userid,'moduleid'=>$moduleid])->select();
        $return=[];
        foreach($data as $k=>$v){
            $return[$v['controller']][]=$v['action'];
        }
        return $return;
    }

}