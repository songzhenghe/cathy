<?php
class frameset extends access{
    public function __construct($prop){
		parent::__construct($prop);
	}
    public function index($request){
        $data=\think\facade\Db::query("select VERSION() as version");
        $mysql_version=$data[0]['version'];
        return $this->tpl(get_defined_vars());
    }

    public function logout($request){
        $request->session()->forget('ADMIN');
        $response = new \Workerman\Protocols\Http\Response(200);
        $response->cookie('PHPSID',null,-1);
        $response->withBody(jump('welcome/login'));

        return $response;
    }
    public function set_menu_current($request){
        $session=$request->session();
        $post = $request->post();
        $session->put([
            'open_sidebar_menu'=>$post['a'],
            'open_sidebar_item'=>$post['href'],
        ]);

        return json_encode([
            'result'=>[
                time()
            ]
        ]);
    }
    public function get_menu_current($request){
        $session=$request->session();
        $a=0;
        if($session->has('open_sidebar_menu')){
            $a=$session->get('open_sidebar_menu');
        }
        $href='#';
        if($session->has('open_sidebar_item')){
            $href=$session->get('open_sidebar_item');
        }
        return json_encode([
            'result'=>['a'=>$a,'href'=>$href]
        ]);
    }
}