<?php
class member extends access{
    public function __construct($prop){
		parent::__construct($prop);
        
	}

    public function _before($request,$response){
        if(!parent::_before($request,$response)) return false;

        $this->moduleid=2;
        $this->MD=$this->CORE_CONF->mod_single($this->moduleid);
        copy_setting($this->moduleid);
        $this->setting=load_setting($this->moduleid);
        $this->getView()->assign(['MODULEID'=>$this->moduleid,'MD'=>$this->MD]);
        return true;
    }

    public function add($request){
        $post = $request->post();
        if($post){
            $post=dtrim($post);
            $username=$post['username'];
            if(!$username){
                return $this->msg(0,'账号不能为空');
            }
            $password=$post['password'];
            if(!$password){
                return $this->msg(0,'密码不能为空');
            }
            $e=\think\facade\Db::name('member')->where(['username'=>$username])->value('userid');
            if($e){
                return $this->msg(0,'账号已经存在');
            }

            $post['password']=md5($post['password']);

            $insertid=\think\facade\Db::name('member')->insertGetId($post);
            return $this->msg($insertid);
        }
        $groupid=0;
        return $this->tpl(get_defined_vars());
    }

    public function lst($request){
        $param = $request->get();
        extract($param);
        $where = [];
        isset($userid) or $userid='';
        if($userid){
            $where[]=['userid','=',$userid];
        }
        isset($username) or $username='';
        if($username){
            $where[]=['username','=',$username];
        }
        isset($groupid) or $groupid=0;
        if($groupid){
            $where[]=['groupid','=',$groupid];
        }
        isset($truename) or $truename='';
        if($truename){
            $where[]=['truename','like',"%{$truename}%"];
        }
        $page = $param['page']??1;
        $pagesize = $this->setting['pagesize'];

        $data=\think\facade\Db::name('member')->where($where)->order('userid','desc')->page($page,$this->setting['pagesize'])->select()->toArray();
        $total = \think\facade\Db::name('member')->where($where)->count('*');
        $pager=pagination($request->uri(),$param,$page,$pagesize,$total);
        return $this->tpl(get_defined_vars());
    }

    public function mod($request){
        $get=$request->get();
        $userid=intval($get['userid']);
        if(!$userid){
            return $this->msg(0);
        }
        $raw=$post=$request->post();
        if($post){
            unset($post['submit'],$post['FORWARD']);
            if(!$post['password']){
                unset($post['password']);
            }else{
                $post['password']=md5($post['password']);
            }

            $r=\think\facade\Db::name('member')->where(['userid'=>$userid])->update($post);
            return $this->msg($r===false?false:true,'',$raw['FORWARD']);
        }

        $data=\think\facade\Db::name('member')->where(['userid'=>$userid])->find();
        if(!$data){
            return $this->msg(0,'数据不存在');
        }
        $FORWARD=$request->header('referer');
        return $this->tpl(get_defined_vars());
    }

    public function del($request){
        $get=$request->get();
        $userid=intval($get['userid']);
        if(!$userid){
            return $this->msg(0);
        }
        $data=\think\facade\Db::name('member')->where(['userid'=>$userid])->find();
        if(!$data){
            return $this->msg(0,'数据不存在');
        }
        $r=\think\facade\Db::name('member')->where(['userid'=>$userid])->update(['groupid'=>3]);
        return $this->msg($r===false?false:true);
    }

    public function outside($request){
        $param = $request->get();
        extract($param);
        if(!isset($callback)){
            return $this->msg(0,'lose callback');
        }
        $where = [];
        isset($userid) or $userid='';
        if($userid){
            $where[]=['userid','=',$userid];
        }
        isset($username) or $username='';
        if($username){
            $where[]=['username','=',$username];
        }
        isset($groupid) or $groupid=0;
        if($groupid){
            $where[]=['groupid','=',$groupid];
        }
        isset($truename) or $truename='';
        if($truename){
            $where[]=['truename','like',"%{$truename}%"];
        }
        $page = $param['page']??1;
        $pagesize = $this->setting['pagesize'];

        $data=\think\facade\Db::name('member')->where($where)->order('userid','desc')->page($page,$this->setting['pagesize'])->select()->toArray();
        $total = \think\facade\Db::name('member')->where($where)->count('*');
        $pager=pagination($request->uri(),$param,$page,$pagesize,$total);
        return $this->tpl(get_defined_vars());
    }

    public function login_log($request){
        $param = $request->get();
        extract($param);

        $where=[];
        isset($username) or $username='';
        if($username){
            $where[]=['username','=',$username];
        }
        isset($loginip) or $loginip='';
        if($loginip){
            $where[]=['loginip','=',$loginip];
        }
        isset($logintime_1) or $logintime_1='';
        if($logintime_1){
            $str=strtotime($logintime_1);
            $where[]=['logintime','>=',$str];
        }
        isset($logintime_2) or $logintime_2='';
        if($logintime_2){
            $str=strtotime($logintime_2.' 23:59:59');
            $where[]=['logintime','<=',$str];
        }
        $page = $param['page']??1;
        $pagesize = $this->setting['pagesize'];

        $data=\think\facade\Db::name('login_back')->where($where)->order('itemid','desc')->page($page,$this->setting['pagesize'])->select();
        $total = \think\facade\Db::name('login_back')->where($where)->count('*');
        $pager=pagination($request->uri(),$param,$page,$pagesize,$total);
        return $this->tpl(get_defined_vars());
    }

    public function setting($request){
        $post = $request->post();
        if($post){
            $r=save_setting($this->moduleid,$post);
            return $this->msg($r);
        }
        $setting=load_setting($this->moduleid);
        return $this->tpl(get_defined_vars());
    }

    public function authenticate($request){
        $get=$request->get();
        extract($get);
        isset($userid) or $userid=0;
		$userid=intval($userid);
		if(!$userid){
            return $this->msg(0,'lose userid');
        }
		isset($moduleid) or $moduleid=1;
		$moduleid=intval($moduleid);
		
		$module_list=$this->CORE_CONF->MODULE;
		if(!array_key_exists($moduleid,$module_list)){
            return $this->msg(0,'非法操作');
        }
		$model=model('access_back');
		
		$post=$request->post();
		if($post){
            $r=$model->set_auth($userid,$post);
            return $this->msg($r);
		}
		
		$mod=$this->CORE_CONF->mod_single($moduleid);
		$checked=$model->get_auth($userid,$moduleid);

        $CORE_CONF=$this->CORE_CONF;
        return $this->tpl(get_defined_vars());
    }

}