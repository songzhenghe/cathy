<?php
class welcome extends controller {
	public function __construct($prop){
		parent::__construct($prop);
	}
	public function index($request){
		return $this->jump('welcome/login');
	}

	public function login($request){
	    $session=$request->session();
		if($session->get('ADMIN')){
			return $this->jump('frameset/index');
		}
		$post = $request->post();
        if($post){
            $username=trim($post['username']);
            $password=trim($post['password']);
            $code=trim($post['code']);
            if(!$username or !$password){
                return $this->msg(0,'账号和密码不能为空');
            }
            if(!$code){
                return $this->msg(0,'验证码不能为空');
            }
            if(strtolower($code)!=strtolower($session->get('CODE'))){
                return $this->msg(0,'验证码不正确');
            }
            $user=\think\facade\Db::name('member')->where(['username'=>$username])->find();
            if(!$user){
                return $this->msg(0,'账号不存在');
            }
            if($user['groupid']!=1 and $user['groupid']!=2){
                return $this->msg(0,'账号已经被禁用');
            }
            //密码 123456
            if(md5($password)!=$user['password']){
                return $this->msg(0,'密码错误');
            }
            \think\facade\Db::name('login_back')->insert([
                'username'=>$username,
                'loginip'=>$request->header('x-real-ip'),
                'logintime'=>time(),
                'agent'=>$request->header('user-agent'),
            ]);

            unset($user['password']);
            //session
			$session->set('ADMIN',$user);
            return $this->msg(1,'登录成功');
		}
		return $this->tpl(get_defined_vars());
    }
    
	
	
}