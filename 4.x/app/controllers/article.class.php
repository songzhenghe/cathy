<?php
require_once APPPATH.'controllers/traits/category.php';
require_once APPPATH.'controllers/traits/upload.php';

class article extends access{

	use category;
    use upload;

	public function __construct($prop){
		parent::__construct($prop);
	}

	public function _before($request,$response){
        if(!parent::_before($request,$response)) return false;

        $this->moduleid=11;
        $this->MD=$this->CORE_CONF->mod_single($this->moduleid);
        \copy_setting($this->moduleid);
        $this->setting=\load_setting($this->moduleid);
        $this->UNIQ=\uniq();
        $this->getView()->assign([
            'MODULEID'=>$this->moduleid,
            'MD'=>$this->MD,
            'CATEGORY_CACHE_URL'=>'?r=article/category_cache',
            'CATEGORY_MOD_URL'=>'?r=article/category_mod',
            'CATEGORY_DEL_URL'=>'?r=article/category_del',
            'UPLOAD_URL1'=>'?r=article/upload_img_action',
            'UPLOAD_URL2'=>'?r=article/upload_file_action',
            'UPLOAD_URL3'=>'?r=article/upload_img_tradition_action',
            'UPLOAD_URL4'=>'?r=article/upload_file_tradition_action',
            'UPLOAD_HUGE'=>'?r=article/upload_huge_action',
            'UNIQ'=>$this->UNIQ,
        ]);
        return true;
    }

	//attachment hook
	protected function save_attachment($articleid,$uniq,$url){
		$model=model('article_attachment');
		$model->insert([
			'articleid'=>$articleid,
			'uniq'=>$uniq,
			'url'=>$url,
			'ts'=>time(),
		]);
	}

	public function add($request){
		
		$post=$request->post();
		$model=model('article');
		if($post){
			$post=\dtrim($post);
			$post['addtime']=time();
			$post['edittime']=time();
			$post['userid']=$this->auth['userid'];
			$post['username']=$this->auth['username'];
			$post['editorid']=$this->auth['userid'];
			$post['editor']=$this->auth['username'];

			if(intval($post['catid'])==0){
                return $this->msg(0,'所属分类不能为空');
			}
			
			if(!$post['title']){
                return $this->msg(0,'标题不能为空');
			}
			
			if(!$post['content']){
                return $this->msg(0,'内容不能为空');
			}

			$insertid=$model->insert($post);
			if($insertid){
				$model2=model('article_attachment');
				$model2->where(['uniq'=>$post['UNIQ']])->update(['articleid'=>$insertid]);
			}
            return $this->msg($insertid);
		}
		$category_model=model('category');
		$category=$category_model->category_select($this->moduleid,'catid',0,'请选择',' class="form-control" ');
		$flag=1;
		return $this->tpl(get_defined_vars());
	}

	public function lst($request){
		$param=$request->get();
		extract($param);
		$model = model('article');
		$category_model=model('category');
		$where = [];

		isset($itemid) or $itemid='';
		if($itemid){
			$where[]=['itemid','=',$itemid];
		}
		isset($catid) or $catid=0;
		if($catid){
			$where[]=['catid','=',$catid];
		}
		isset($title) or $title='';
		if($title){
			$where[]=['title','like',"%{$title}%"];
		}
		isset($flag) or $flag=0;
		if($flag){
			$where[]=['flag','=',$flag];
		}
		$page = $param['page']??1;
		$pagesize = $this->setting['pagesize'];

        $data=\think\facade\Db::name('article')->where($where)->order('itemid','desc')->page($page,$this->setting['pagesize'])->select()->toArray();
		foreach($data as $k=>$v){
			$data[$k]['cat']=$category_model->get_category($v['catid']);
			$data[$k]['addtime']=\timeswitch($v['addtime']);
		}
        $total = \think\facade\Db::name('article')->where($where)->count('*');
        $pager=pagination($request->uri(),$param,$page,$pagesize,$total);
		$category=$category_model->category_select($this->moduleid,'catid',$catid,'请选择',' class="form-control" ');
		return $this->tpl(get_defined_vars());
	}

	public function mod($request){
		
		$get=$request->get();
		extract($get);
		$itemid=intval($itemid);
		if(!$itemid){
            return $this->msg(0,'lose itemid');
		}

		$raw=$post=$request->post();
		$model=model('article');
		if($post){
			unset($post['submit'],$post['UNIQ'],$post['FORWARD']);
			$post=\dtrim($post);
			$post['edittime']=time();
			$post['editorid']=$this->auth['userid'];
			$post['editor']=$this->auth['username'];

			if(intval($post['catid'])==0){
                return $this->msg(0,'所属分类不能为空');
			}
			
			if(!$post['title']){
                return $this->msg(0,'标题不能为空');
			}
			
			if(!$post['content']){
                return $this->msg(0,'内容不能为空');
			}
			
			$r=$model->where(['itemid'=>$itemid])->update($post);
			if($r){
				$model2=model('article_attachment');
				$model2->where(['uniq'=>$raw['UNIQ']])->update(['articleid'=>$itemid]);
				$model->treat_attachment($itemid);
			}
            return $this->msg($r,'',$raw['FORWARD']);
		}
		$data=$model->where(['itemid'=>$itemid])->find();
		if(!$data){
            return $this->msg(0,'数据不存在');
		}
		$category_model=model('category');
		$category=$category_model->category_select($this->moduleid,'catid',$data['catid'],'请选择',' class="form-control" ');
		$FORWARD=$request->header('referer');
		return $this->tpl(get_defined_vars());
	}

	public function del($request){
		
		$get=$request->get();
		extract($get);

		$itemid=intval($itemid);
		if(!$itemid){
			$this->msg(0,'lose itemid');
			return;
		}
		$model=model('article');
		$d=$model->where(['itemid'=>$itemid])->find();
		if(!$d){
            return $this->msg(0,'数据不存在');
		}
		//删除thumb
		//删除附件
		if(false){
			$model=model('article_attachment');
			$attachments=$model->where(['articleid'=>$itemid])->select()->toArray();
			foreach($attachments as $k=>$v){
				$file=ROOTPATH.'public'.$v['url'];
				if(!file_exists($file)) continue;
				unlink($file);
			}
			$model->delete(['articleid'=>$itemid]);
		}
		$r=$model->where(['itemid'=>$itemid])->update(['flag'=>3]);//$d->destroy();
        return $this->msg($r===false?false:true);
	}

	public function preview($request){
		
		$get=$request->get();
		extract($get);
		$itemid=intval($itemid);
		if(!$itemid){
            return $this->msg(0,'lose itemid');
		}
		$model=model('article');
		$data=$model->where(['itemid'=>$itemid])->find();
		if(!$data){
            return $this->msg(0,'数据不存在');
		}
		$category_model=model('category');
		$cat=$category_model->get_category($data['catid']);
		return $this->tpl(get_defined_vars());
	}

	public function outside($request){
		$param=$request->get();
		extract($param);

		if(!$callback){
            return $this->msg(0,'lose callback');
		}

		$model = model('article');
		$category_model=model('category');

        $where = [];

        isset($itemid) or $itemid='';
        if($itemid){
            $where[]=['itemid','=',$itemid];
        }
        isset($catid) or $catid=0;
        if($catid){
            $where[]=['catid','=',$catid];
        }
        isset($title) or $title='';
        if($title){
            $where[]=['title','like',"%{$title}%"];
        }
        isset($flag) or $flag=0;
        if($flag){
            $where[]=['flag','=',$flag];
        }

		$page = $param['page']??1;
		$pagesize = $this->setting['pagesize'];

        $data=\think\facade\Db::name('article')->where($where)->order('itemid','desc')->page($page,$this->setting['pagesize'])->select()->toArray();
        foreach($data as $k=>$v){
            $data[$k]['cat']=$category_model->get_category($v['catid']);
            $data[$k]['addtime']=\timeswitch($v['addtime']);
        }
        $total = \think\facade\Db::name('article')->where($where)->count('*');
        $pager=pagination($request->uri(),$param,$page,$pagesize,$total);
		$category=$category_model->category_select($this->moduleid,'catid',$catid,'请选择',' class="form-control" ');
		return $this->tpl(get_defined_vars());
	}

	public function setting($request){
		
		$post = $request->post();
		if($post){
			$r=\save_setting($this->moduleid,$post);
			return $this->msg($r);
		}
		$setting=\load_setting($this->moduleid);
		return $this->tpl(get_defined_vars());
	}
	public function clear_attachments($request){
		$model=model('article_attachment');
		$where=[];
		$where[]=['articleid','=',0];
		$where[]=['ts','<',time()-86400];
		$attachments=$model->where($where)->select()->toArray();
		foreach($attachments as $k=>$v){
			$file=ROOTPATH.'public'.$v['url'];
			if(!file_exists($file)) continue;
			unlink($file);
		}
		$model->where($where)->delete();
		return $this->msg(1);
	}

	public function test($request){
        
        return $this->tpl(get_defined_vars());
    }
	
}