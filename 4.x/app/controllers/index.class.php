<?php

class index extends controller
{
    public function __construct($prop)
    {
        parent::__construct($prop);
    }

    public function welcome()
    {
        return $this->jump('welcome/index');
    }


    public function code($request)
    {
        //5.创建一个验证码

        $code_length = 4;

        $codes = '';


        for ($i = 0; $i < $code_length; $i++) {

            $codes .= dechex(mt_rand(0, 15));

        }


        //7.生成sesstion
        $request->session()->set('CODE',$codes);


        $width = 75;

        $height = 25;


        //1. 创建一张图片

        $img = imagecreatetruecolor($width, $height);


        //4. 创建图片静态内容

        //白色

        $white = imagecolorallocate($img, 255, 255, 255);

        //黑色

        $black = imagecolorallocate($img, 100, 100, 100);

        //红色

        $red = imagecolorallocate($img, 255, 0, 0);


        //图片背景

        imagefill($img, 0, 0, $white);


        //随机线条

        for ($i = 0; $i < 4; $i++) {

            //创建随机颜色

            $img_mt_color = imagecolorallocate($img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));


            //画线

            imageline($img, mt_rand(0, $width), mt_rand(0, $height), mt_rand(0, $width), mt_rand(0, $height), $img_mt_color);

        }


        //随机打雪花

        for ($i = 0; $i < 100; $i++) {

            //创建随机浅淡色

            $img_mt_color = imagecolorallocate($img, mt_rand(200, 255), mt_rand(200, 255), mt_rand(200, 255));


            //画*

            imagestring($img, 1, mt_rand(0, $width), mt_rand(0, $height), '*', $img_mt_color);

        }


        //imagestring($img, 3, 10, 10, strlen($codes), $red);


        //6.绘制验证码

        for ($i = 0; $i < $code_length; $i++) {

            //创建验证码颜色（偏深色）

            $img_mt_color = imagecolorallocate($img, mt_rand(0, 100), mt_rand(0, 150), mt_rand(0, 200));

            //生成验证码每一位字符

            imagestring($img, mt_rand(3, 5), $i * $width / 4 + mt_rand(0, 10), mt_rand(1, $height / 2), $codes[$i], $img_mt_color);

        }


        //创建红色矩形边框

        imagerectangle($img, 0, 0, $width - 1, $height - 1, $red);


        //2. 让浏览器知道输出的是一张图片
        $response=new \Workerman\Protocols\Http\Response(200);
        $response->header('Content-Type', 'image/png');

        //3. 输出
        ob_start();
        imagepng($img);
        $c = ob_get_contents();
        ob_end_clean();
        $response->withBody($c);
        //4. 销毁

        imagedestroy($img);
        return $response;
    }

}