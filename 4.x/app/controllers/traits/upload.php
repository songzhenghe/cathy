<?php
Trait upload{

    protected function upload_file_common($request,$extensions){
        $post=$request->post();
        if($post){
            $class=$post['class'];
            $i=$post['i'];
            $callback=$post['callback'];
            $uniq=$post['uniq'];
            //file_put_contents('a.php',var_export($_FILES,true));
            $config=array(
				'field_name'=>'file',
				'upload_path'=>PUBLICPATH.'uploads/',
				'allow_type'=>$extensions,//数组
				'max_size'=>'2',
				'unit'=>'M',
				'israndname'=>true,
				'iscreatedir'=>true,
				'iscreatebydate'=>true,
				'isoverride'=>true
			);
			$upload=new sys_upload();
            $r=$upload->upload_instance($request->file(), $config);

            if($r['error_number']<0){
                return json_encode([
                    'result'=>['code'=>1,'msg'=>$r['error_message'],'url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
                ]);
            }
            $url='/uploads/'.$r['file_relpath'].$r['new_name'];
            //save to db
            if(method_exists($this,'save_attachment')){
                $this->save_attachment(0,$uniq,$url);
            }
            
            return json_encode([
                'result'=>['code'=>0,'msg'=>'success','url'=>$url,'class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
    }
    public function upload_img($request){
        $get=$request->get();
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            return $this->msg(0,'lose class/i/callback/uniq');
        }
        return $this->tpl(get_defined_vars(),'upload/img');
    }
    public function upload_img_action($request){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
        ];
        return $this->upload_file_common($request,$extensions);
    }
    public function upload_file($request){
        $get=$request->get();
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            return $this->msg(0,'lose class/i/callback/uniq');
        }
        return $this->tpl(get_defined_vars(),'upload/file');
    }
    public function upload_file_action($request){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
            '.rar',
            '.zip',
            '.pdf',
            '.txt',
            '.doc',
            '.docx',
            '.ppt',
            '.pptx',
            '.xls',
            '.xlsx',
            '.swf',
        ];
        return $this->upload_file_common($request,$extensions);
    }

    private function mkhtml($fn,$fileurl,$message)
	{
		$str='<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$fn.', \''.$fileurl.'\', \''.$message.'\');</script>';
		return $str;
    }
    
    protected function ck_common($request,$extensions){
        $post=$request->post();
        if(!$post){
            return $this->msg(0);
        }
        $get=$request->get();
        extract($get);
        if(empty($CKEditorFuncNum)){
            return $this->msg(0,'错误的功能调用请求！');
        }
        if(!isset($uniq)){
            return $this->mkhtml(1,'','lose uniq!');
        }

        $config=array(
            'field_name'=>'upload',
            'upload_path'=>PUBLICPATH.'uploads/',
            'allow_type'=>$extensions,//数组
            'max_size'=>'2',
            'unit'=>'M',
            'israndname'=>true,
            'iscreatedir'=>true,
            'iscreatebydate'=>true,
            'isoverride'=>true
        );
        $upload=new sys_upload();
        $r=$upload->upload_instance($request->file(),$config);

        if($r['error_number']<0){
            return $this->mkhtml($CKEditorFuncNum,'',$r['error_message']);
        }
        $url='/uploads/'.$r['file_relpath'].$r['new_name'];
        //save to db
        if(method_exists($this,'save_attachment')){
            $this->save_attachment(0,$uniq,$url);
        }
        return $this->mkhtml($CKEditorFuncNum,$url,'上传成功！');
    }

    public function upload_ck_img($request){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
        ];
        return $this->ck_common($request,$extensions);
    }

    public function upload_ck_file($request){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
            '.rar',
            '.zip',
            '.pdf',
            '.txt',
            '.doc',
            '.docx',
            '.ppt',
            '.pptx',
            '.xls',
            '.xlsx',
            '.swf',
        ];
        return $this->ck_common($request,$extensions);
    }

    public function upload_huge($request){
        $get=$request->get();
        extract($get);
        
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            return $this->msg(0,'lose class/i/callback/uniq');
        }
        return $this->tpl(get_defined_vars(),'upload/huge');
    }

    public function upload_huge_action($request){
        $session=$request->session();
        $post=$request->post();
        $name=$post['name'];
        $total=$post['total'];
        $index=$post['index'];
        $fileSize=$post['fileSize'];

        $class=$post['class'];
        $i=$post['i'];
        $callback=$post['callback'];
        $uniq=$post['uniq'];

        $file=$request->file('data');
        //主要判断文件大小和类型
        if($fileSize>2*1024*1024*1024){
            return json_encode([
                'result'=>['code'=>101,'msg'=>'文件超出最大大小','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        $ext=pathinfo($name,PATHINFO_EXTENSION);
        $extensions=[
            'jpg',
            'png',
            'gif',
            'rar',
            'zip',
            'pdf',
            'txt',
            'doc',
            'docx',
            'ppt',
            'pptx',
            'xls',
            'xlsx',
            'swf',
			'iso',
        ];
        if(!in_array($ext,$extensions)){
            return json_encode([
                'result'=>['code'=>102,'msg'=>'文件类型不正确','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        $DEST=$session->has('UPLOAD_HUGE_DEST')?$session->get('UPLOAD_HUGE_DEST'):'';
        if($DEST){
            $destination=$DEST;
        }else{
            mt_srand();
            $destination=PUBLICPATH.'uploads/'.date('Y/m/d').'/'.time().rand(1000,9999).'.'.$ext;
            $session->set('UPLOAD_HUGE_DEST',$destination);
        }
        
        if(!file_exists(dirname($destination))){
            mkdir(dirname($destination),0777,true);
        }
        $url=str_replace(ROOTPATH.'public','',$destination);

        //如果第一次上传的时候，该文件已经存在，则删除文件重新上传
        if($index==1 && file_exists($destination) && filesize($destination)==$fileSize){
            unlink($destination);
        }
        //合并文件
        $data=file_get_contents($file['tmp_name']);
        $r=file_put_contents($destination, $data, FILE_APPEND);
        if(!$r){
            return json_encode([
                'result'=>['code'=>103,'msg'=>'文件写入失败','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        //最后一块时
        if($index==$total){
            //save to db
            if(method_exists($this,'save_attachment')){
                $this->save_attachment(0,$uniq,$url);
            }
            $session->forget('UPLOAD_HUGE_DEST');
            return json_encode([
                'result'=>['code'=>0,'msg'=>'success','url'=>$url,'class'=>$class,'i'=>$i,'callback'=>$callback]
            ]);
        }
        //继续上传
        return json_encode([
            'result'=>['code'=>0,'msg'=>'success','url'=>'','class'=>$class,'i'=>$i,'callback'=>$callback]
        ]);
    }
    public function upload_img_tradition($request){
        $get=$request->get();
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            return $this->msg(0,'lose class/i/callback/uniq');
        }
        return $this->tpl(get_defined_vars(),'upload/img_tradition');
    }
    public function upload_img_tradition_action($request){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
        ];
        return $this->upload_file_common($request,$extensions);
    }
    public function upload_file_tradition($request){
        $get=$request->get();
        extract($get);
        //$class $i $callback;
        //$class='demo';
        //$i=0;
        //$callback='cb_thumb';
        if(!isset($class) or !isset($i) or !isset($callback) or !isset($uniq)){
            return $this->msg(0,'lose class/i/callback/uniq');
        }
        return $this->tpl(get_defined_vars(),'upload/file_tradition');
    }
    public function upload_file_tradition_action($request){
        $extensions=[
            '.jpg',
            '.png',
            '.gif',
            '.rar',
            '.zip',
            '.pdf',
            '.txt',
            '.doc',
            '.docx',
            '.ppt',
            '.pptx',
            '.xls',
            '.xlsx',
            '.swf',
        ];
        return $this->upload_file_common($request,$extensions);
    }



}