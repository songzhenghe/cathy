<?php
trait category{

    public function category_add($request){
        
        $post=$request->post();
        $model=model('category');
        if($post){
            $post=\dtrim($post);
            $post['moduleid']=$this->moduleid;
            $post['listorder']=intval($post['listorder']);

            
            if(!$post['moduleid']){
                return $this->msg(0,'mouleid不能为空');
            }

            if(!$post['catname']){
                return $this->msg(0,'分类名称不能为空');
            }

            $e=$model->where(['moduleid'=>$post['moduleid'],'parentid'=>$post['parentid'],'catname'=>$post['catname']])->find();
            if($e){
                return $this->msg(0,'分类名称已经存在');
            }

            $insertid=$model->insert($post);
            $model->cache($this->moduleid);
            return $this->msg($insertid);
        }
        $parent=$model->category_select($this->moduleid,'parentid',0,'顶级分类',' class="form-control" ');
        $max_catid=$model->max_catid()+1;
        return $this->tpl(get_defined_vars(),'category/category_add');
    }

    public function category_lst($request){
        
        $get=$request->get();
        extract($get);

        $cond=[];
		$cond['moduleid']=$this->moduleid;
        
        $model=model('category');
        $category_arr=$model->where($cond)->select();
		$category_arr=$model->getCatTree($category_arr);

        return $this->tpl(get_defined_vars(),'category/category_lst');
    }

    public function category_mod($request){
        
        $get=$request->get();
        extract($get);
        $catid=intval($catid);
		if(!$catid){
            return $this->msg(0,'lose catid');
        }

        $raw=$post=$request->post();
        $model=model('category');
        if($post){
            unset($post['submit'],$post['FORWARD']);
            $post=\dtrim($post);
            $post['moduleid']=$this->moduleid;
            $post['listorder']=intval($post['listorder']);

            if(!$post['moduleid']){
                return $this->msg(0,'mouleid不能为空');
            }

            if(!$post['catname']){
                return $this->msg(0,'分类名称不能为空');
            }

            $where=[];
            $where[]=['catid','<>',$catid];
            $where[]=['moduleid','=',$post['moduleid']];
            $where[]=['parentid','=',$post['parentid']];
            $where[]=['catname','=',$post['catname']];
            $e=$model->where($where)->find();
            if($e){
                return $this->msg(0,'分类名称已经存在');
            }


            $r=$model->mod_check_ok($this->moduleid,$catid,$post['parentid']);
			if(!$r){
                return $this->msg(0,'上级分类选择错误');
            }
            $r=$model->where(['catid'=>$catid])->update($post);
            $model->cache($this->moduleid);
            return $this->msg($r===false?false:true,'',$raw['FORWARD']);
        }
        $data=$model->where(['moduleid'=>$this->moduleid,'catid'=>$catid])->find();
		if(!$data){
            return $this->msg(0,'分类不存在');
        }
        $parent=$model->category_select($this->moduleid,'parentid',$data['parentid'],'顶级分类',' class="form-control" ');
		$FORWARD=$request->header('referer');
        return $this->tpl(get_defined_vars(),'category/category_mod');
    }

    public function category_del($request){
        
        $get=$request->get();
        extract($get);

        $catid=intval($catid);
		if(!$catid){
            return $this->msg(0,'lose catid');
        }
		$model=model('category');
		$r=$model->del_check_ok($catid,$this->moduleid);
        if(!$r){
            return $this->msg(0,'该分类无法删除，请先删除其子分类，并删除其下文章');
        }
        $cat=$model->where(['moduleid'=>$this->moduleid,'catid'=>$catid])->find();
        if(!$cat){
            return $this->msg(0,'数据不存在');
        }
        $r=$model->where(['moduleid'=>$this->moduleid,'catid'=>$catid])->delete();
		$model->cache($this->moduleid);
		return $this->msg($r===false?false:true);
    }

    public function category_cache($request){
        $model=model('category');
        $model->cache($this->moduleid);
        return $this->msg(1);
    }

    public function category_outside($request){
        
        $get=$request->get();
        extract($get);

        if(!isset($callback)){
            return $this->msg(0,'lose callback');
        }

        $cond=[];
		$cond['moduleid']=$this->moduleid;
        
        $model=model('category');
        $category_arr=$model->where($cond)->select();
		$category_arr=$model->getCatTree($category_arr);

        return $this->tpl(get_defined_vars(),'category/category_outside');
    }

}