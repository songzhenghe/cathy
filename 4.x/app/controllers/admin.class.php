<?php
class admin extends access{

    public function __construct($prop){
		parent::__construct($prop);
        
	}

    public function _before($request,$response){
        if(!parent::_before($request,$response)) return false;

        $this->moduleid=1;
        $this->MD=$this->CORE_CONF->mod_single($this->moduleid);
		copy_setting($this->moduleid);
        $this->setting=load_setting($this->moduleid);
        $this->getView()->assign(['MODULEID'=>$this->moduleid,'MD'=>$this->MD]);
        return true;
    }

    public function safe($request){
        $post = $request->post();
        if($post){
            $old_password=trim($post['old_password']);
            $new_password=trim($post['new_password']);
            $new_password2=trim($post['new_password2']);
            if(!$old_password or !$new_password or !$new_password2){
                return $this->msg(0,'请填写完密码后再提交');
            }
            if($new_password!=$new_password2){
                return $this->msg(0,'两次新密码不一致');
            }
            $userid=$this->auth['userid'];
            $user=\think\facade\Db::name('member')->where(['userid'=>$userid])->find();
            if(md5($old_password)!=$user['password']){
                return $this->msg(0,'旧密码不正确');
            }

            $affected_rows=\think\facade\Db::name('member')->where(['userid'=>$userid])->update([
                'password'=>md5($new_password),
            ]);

            return $this->msg($affected_rows===false?false:true);
        }

        return $this->tpl(get_defined_vars());
    }

    public function setting($request){
        $post = $request->post();
        if($post){
            $r=save_setting($this->moduleid,$post);
            return $this->msg($r);
        }
        $setting=load_setting($this->moduleid);
        return $this->tpl(get_defined_vars());
    }


}