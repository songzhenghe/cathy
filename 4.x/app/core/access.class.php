<?php
class access extends controller{
    public $userid;
    public $username;

    protected $auth;
    protected $moduleid=0;
    public function __construct($prop){
        parent::__construct($prop);
    }

    function _before($request,$response){
        $session=$request->session();
        parent::_before($request,$response);
        //检查是否登录
        if(!$session->get('ADMIN')){
            $this->CONN->close($this->msg(0,'非法操作','welcome/login'));
            return false;
        }
        $ADMIN=$session->get('ADMIN');
        $this->userid=$ADMIN['userid'];
        $this->username=$ADMIN['username'];
        $this->auth=$ADMIN;

        //全局模板变量
        $this->getView()->assign(['ADMIN'=>$this->auth]);

        //check module is exist
        if(in_array($this->MODULE,['frameset','upload','common'])){
            return true;
        }
        if(!isset($this->CORE_CONF->MAP[$this->MODULE])){
            $this->CONN->close($this->msg(0,'模块不存在'));
            return false;
        }
        $this->moduleid=$moduleid=$this->CORE_CONF->MAP[$this->MODULE];
        //check rule is exist
        if(!$this->CORE_CONF->rule_exists($moduleid,$this->ACTION)){
            $this->CONN->close($this->msg(0,$this->MODULE.':'.$this->ACTION.'权限规则不存在'));
            return false;
        }
        if($this->auth['groupid']==1){
            return true;
        }
        if(!$this->CORE_CONF->MODULE[$moduleid]['is_auth']){
            return true;
        }
        if(!$this->CORE_CONF->AUTH[$moduleid][$this->ACTION]['is_auth']){
            return true;
        }
        //check if has right
        $d=\think\facade\Db::name('access_back')->where(['userid'=>$this->auth['userid'],'moduleid'=>$moduleid,'module'=>$this->MODULE,'controller'=>$this->MODULE,'action'=>$this->ACTION])->value('itemid');
        if(!$d){
            $this->CONN->close($this->msg(0,'没有权限'));
            return false;
        }

        return true;
    }
    function _after($request,$response){

    }


}