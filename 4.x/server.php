<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;
use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Request;
use Workerman\Protocols\Http\Response;
use Workerman\Timer;
use \Workerman\Protocols\Http\Session\FileSessionHandler;
use think\facade\Db;
use Naroga\RedisCache\Redis;
use Predis\Client;

define('DEBUG', true);
define('ROOTPATH', dirname(__FILE__).'/');
define('BASEPATH', ROOTPATH.'framework/');
define('APPPATH', ROOTPATH.'app/');
define('RUNTIMEPATH', ROOTPATH.'runtime/');
define('LOGPATH', RUNTIMEPATH.'log/');
define('PUBLICPATH', ROOTPATH.'public/');

//包含配置文件
$CFG=require_once dirname(__FILE__).'/config.inc.php';
//读取controller目录
// $CONTROLLERS=[];
// if ($handle = opendir(APPPATH.'controllers/')) {
//     while (false !== ($file = readdir($handle))) {
//         if ($file=='.' || $file=='..' || is_dir(APPPATH.'controllers/'.$file)) {
//             continue;
//         }
//         $CONTROLLERS[]=str_replace('.class.php', '', $file);
//     }
//     closedir($handle);
// }
// print_r($CONTROLLERS);

if (strtoupper(substr(PHP_OS, 0, 3))!=='WIN') {
    require_once BASEPATH.'FileMonitor.php';
}

FileSessionHandler::sessionSavePath(RUNTIMEPATH.'session/');
$http_worker = new Worker($CFG['base']['listen']);
$http_worker->name=$CFG['base']['name'];
Worker::$pidFile = RUNTIMEPATH.'/workerman.pid';
Worker::$logFile = LOGPATH.'workerman.log';
Worker::$stdoutFile = LOGPATH.'stdout.log';
// 4 processes
$http_worker->count = $CFG['base']['count'];

$http_worker->onWorkerStart = function (Worker $worker) {
    global $CFG;
    //错误处理
//    if (defined('DEBUG') and DEBUG) {
//        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
//            echo "在{$errfile}的第{$errline}行发生了错误：{$errno} {$errstr}\n";
//        });
//    } else {
//        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
//            $str="在{$errfile}的第{$errline}行发生了错误：{$errno} {$errstr}\n";
//            file_put_contents(LOGPATH.'error_log.txt', $str, FILE_APPEND);
//        });
//    }
    //设置时区
    date_default_timezone_set('Asia/Shanghai');
    //引入自定义函数文件
    require_once BASEPATH.'functions.inc.php';

    spl_autoload_register('my_autoloader');
    //引入项目自定义函数文件
    require_once APPPATH.'functions.inc.php';
    //包含controller
    require_once BASEPATH.'controller.class.php';

    // 数据库配置信息设置（全局有效）
    //https://www.kancloud.cn/manual/think-orm/1257999
    try {
        $redis = new Redis(new Client($CFG['cache']));
        Db::setCache($redis);
        Db::setConfig($CFG['db']);
        Db::execute("SET sql_mode=''");
        // 维持mysql心跳
        if ($worker) {
            Timer::add(55, function () use ($CFG) {
                $connections = $CFG['db']['connections'];
                foreach ($connections as $key => $item) {
                    if ($item['type'] == 'mysql') {
                        Db::connect($key)->query('select 1');
                    }
                }
            });
        }
    } catch (Exception $e) {
        echo $e->getMessage()."\n";
    }
};

$http_worker->onConnect = function (TcpConnection $connection) {
};

$http_worker->onClose = function (TcpConnection $connection) {
};

$http_worker->onMessage = function (TcpConnection $connection, Request $request) {
    global $CFG;
    //设置字符集
    $response = new Response(200);
    $response->header('Content-Type', 'text/html;charset=UTF-8');

    try {
        $R = $request->get('r');
        isset($R) or $R='index/welcome';

        //处理路由
        $route=explode('/', trim($R, '/'));
        $route=array_map('trim', $route);
        $route=array_map('strtolower', $route);

        // echo "visit {$route[0]}/{$route[1]}\n";

        if (!(isset($route[0]) && $route[0])) {
            $connection->close(error_404('route:0'));
            return;
        }

        if (!(isset($route[1]) && $route[1])) {
            $connection->close(error_404('route:1'));
            return;
        }
        
        //调用app的控制器
        $class=APPPATH.'controllers/'.$route[0].'.class.php';
        if (!file_exists($class)) {
            $connection->close(error_404('file error'));
            return;
        }
        require_once $class;

        //排除一些魔术方法
        $deny_list=array('_before','_after','__construct','__destruct','__call','__callStatic','__get','__set','__isset','__unset','__sleep','__wakeup','__toString','__invoke','__set_state','__clone');
        if (in_array($route[1], $deny_list)) {
            $connection->close(error_404());
            return;
        }

        //add prop
        $prop['R']=$R;
        $prop['MODULE']=$route[0];
        $prop['ACTION']=$route[1];
        $prop['TPL']=APPPATH.'views/'.$route[0].'/'.$route[1].'.html';
        $prop['CFG']=$CFG;
        $prop['CONN']=$connection;
        $prop['REQUEST']=$request;
        $prop['RESPONSE']=$response;

        $object=new $route[0]($prop);

        //判断方法是否存在
        if (!method_exists($object, $route[1])) {
            $connection->close(error_404());
            return;
        }

        //利用反射类判断方法是否可以调用
        $ref=new ReflectionMethod($object, $route[1]);
        if (!$ref->isPublic()) {
            $connection->close(error_404());
            return;
        }


        //调用类下的方法
        $func=$route[1];

        if (method_exists($object, '_before')) {
            $r=$object->_before($request, $response);
        }
        if ($r===false) {
            return;
        }

        $d=$object->$func($request, $response);

        if ($d===false) {
            return;
        }
        if (method_exists($object, '_after')) {
            $object->_after($request, $response);
        }

        if (!$d) {
            $d='ok '.time();
        }
        $connection->close($d);
    } catch (Exception $e) {
        if (defined('DEBUG') and DEBUG) {
            $connection->send($e->getMessage());
        } else {
            file_put_contents(LOGPATH.'error_log.txt', $e->getMessage(), FILE_APPEND);
            $response = new Response(302, ['Location' => '/error/500.html']);
            $connection->send($response);
        }
    }
};

$http_worker->onError = function (TcpConnection $connection, $code, $msg) {
    echo "error $code $msg\n";
};

// 运行worker
Worker::runAll();
